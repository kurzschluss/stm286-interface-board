EESchema Schematic File Version 4
LIBS:PNP_286er_zu_STM32_V01-cache
EELAYER 26 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_02x25_Counter_Clockwise J1
U 1 1 5CA796CD
P 1450 4900
F 0 "J1" H 1500 6250 100 0000 C CNN
F 1 "Conn_02x25_Counter_Clockwise" H 1500 6226 50  0001 C CNN
F 2 "Connectors:HE10-50D" H 1450 4900 50  0001 C CNN
F 3 "~" H 1450 4900 50  0001 C CNN
	1    1450 4900
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x25_Counter_Clockwise J12
U 1 1 5CA7975D
P 15050 4900
F 0 "J12" H 15100 6250 100 0000 C CNN
F 1 "Conn_02x25_Counter_Clockwise" H 15100 6226 50  0001 C CNN
F 2 "Connectors:HE10-50D" H 15050 4900 50  0001 C CNN
F 3 "~" H 15050 4900 50  0001 C CNN
	1    15050 4900
	1    0    0    -1  
$EndComp
Text Label 2200 3700 2    50   ~ 0
NC1
Text Label 2200 3800 2    50   ~ 0
GND
Text Label 2200 3900 2    50   ~ 0
PAT-L
Text Label 2200 4000 2    50   ~ 0
SPOT
Text Label 2200 4100 2    50   ~ 0
0.8_VAC
Text Label 2200 4200 2    50   ~ 0
0.8_HEAD
Text Label 2200 4300 2    50   ~ 0
STROBE
Text Label 2200 4400 2    50   ~ 0
DATA-4
Text Label 2200 4500 2    50   ~ 0
DATA-2
Text Label 2200 4600 2    50   ~ 0
DATA-1
Text Label 2200 4700 2    50   ~ 0
NC2
Text Label 2200 4800 2    50   ~ 0
GND
Text Label 2200 4900 2    50   ~ 0
SP_A
Text Label 2200 5000 2    50   ~ 0
SP_B
Text Label 2200 5100 2    50   ~ 0
SP_C
Text Label 2200 5200 2    50   ~ 0
SP_D
Text Label 2200 5300 2    50   ~ 0
SUPPORTER
Text Label 2200 5400 2    50   ~ 0
LOCATOR
Text Label 2200 5500 2    50   ~ 0
STOPPER
Wire Wire Line
	2200 5300 1750 5300
Wire Wire Line
	1750 5400 2200 5400
Wire Wire Line
	2200 5500 1750 5500
Wire Wire Line
	1750 3700 2200 3700
Wire Wire Line
	1750 3800 2200 3800
Wire Wire Line
	1750 3900 2200 3900
Wire Wire Line
	1750 4000 2200 4000
Wire Wire Line
	1750 4100 2200 4100
Wire Wire Line
	1750 4200 2200 4200
Wire Wire Line
	1750 4300 2200 4300
Wire Wire Line
	1750 4400 2200 4400
Wire Wire Line
	1750 4500 2200 4500
Wire Wire Line
	1750 4600 2200 4600
Wire Wire Line
	1750 4700 2200 4700
Wire Wire Line
	1750 4800 2200 4800
Wire Wire Line
	1750 4900 2200 4900
Wire Wire Line
	1750 5000 2200 5000
Wire Wire Line
	1750 5100 2200 5100
Wire Wire Line
	1750 5200 2200 5200
Wire Wire Line
	1750 5600 2200 5600
Text Label 2200 5600 2    50   ~ 0
CVY-M
Text Label 2200 5700 2    50   ~ 0
+5V
Text Label 2200 5800 2    50   ~ 0
NC3
Text Label 2200 5900 2    50   ~ 0
SP_1
Text Label 2200 6000 2    50   ~ 0
SP_2
Text Label 2200 6100 2    50   ~ 0
SP_3
Wire Wire Line
	1750 5700 2200 5700
Wire Wire Line
	2200 5800 1750 5800
Wire Wire Line
	1750 5900 2200 5900
Wire Wire Line
	2200 6000 1750 6000
Wire Wire Line
	1750 6100 2200 6100
Text Label 800  6100 0    50   ~ 0
SP_4
Text Label 800  6000 0    50   ~ 0
SP_5
Text Label 800  5900 0    50   ~ 0
BAD_MARK
Text Label 800  5800 0    50   ~ 0
SP_6
Text Label 800  5700 0    50   ~ 0
SP_7
Wire Wire Line
	800  5700 1250 5700
Wire Wire Line
	800  5800 1250 5800
Wire Wire Line
	1250 5900 800  5900
Wire Wire Line
	800  6000 1250 6000
Wire Wire Line
	1250 6100 800  6100
Text Label 800  5600 0    50   ~ 0
+5V
Text Label 800  5500 0    50   ~ 0
NC_4
Text Label 800  5400 0    50   ~ 0
SP_8
Wire Wire Line
	800  5400 1250 5400
Wire Wire Line
	1250 5500 800  5500
Wire Wire Line
	800  5600 1250 5600
Text Label 800  5300 0    50   ~ 0
PIN_UP
Text Label 800  5200 0    50   ~ 0
PCB_OUT
Text Label 800  5100 0    50   ~ 0
PCB_IN
Text Label 800  4900 0    50   ~ 0
CCW_SW
Text Label 800  5000 0    50   ~ 0
PCB_DET
Text Label 800  4800 0    50   ~ 0
CW_SW
Text Label 800  4700 0    50   ~ 0
M_CENTER
Text Label 800  3700 0    50   ~ 0
Y_CCW
Text Label 800  3800 0    50   ~ 0
Y_CW
Text Label 800  3900 0    50   ~ 0
GND
Text Label 800  4000 0    50   ~ 0
+5V
Text Label 800  4100 0    50   ~ 0
Y_L1
Text Label 800  4200 0    50   ~ 0
Y_L2
Text Label 800  4300 0    50   ~ 0
Y_L3
Text Label 800  4400 0    50   ~ 0
Y_L4
Text Label 800  4500 0    50   ~ 0
Y_HM
Text Label 800  4600 0    50   ~ 0
SELCT(GND)
Wire Wire Line
	800  3700 1250 3700
Wire Wire Line
	800  3800 1250 3800
Wire Wire Line
	800  3900 1250 3900
Wire Wire Line
	800  4000 1250 4000
Wire Wire Line
	800  4100 1250 4100
Wire Wire Line
	800  4200 1250 4200
Wire Wire Line
	800  4300 1250 4300
Wire Wire Line
	800  4400 1250 4400
Wire Wire Line
	800  4500 1250 4500
Wire Wire Line
	800  4600 1250 4600
Wire Wire Line
	800  4700 1250 4700
Wire Wire Line
	800  4800 1250 4800
Wire Wire Line
	800  4900 1250 4900
Wire Wire Line
	800  5000 1250 5000
Wire Wire Line
	800  5100 1250 5100
Wire Wire Line
	800  5200 1250 5200
Wire Wire Line
	800  5300 1250 5300
Wire Notes Line style solid
	1250 3700 1200 3750
Wire Notes Line style solid
	1200 3650 1250 3700
Wire Notes Line style solid
	1250 3800 1200 3850
Wire Notes Line style solid
	1200 3750 1250 3800
Wire Notes Line style solid
	1200 3650 1200 3850
Wire Notes Line style solid
	1200 4100 1250 4050
Wire Notes Line style solid
	1250 4150 1200 4100
Wire Notes Line style solid
	1200 4200 1250 4150
Wire Notes Line style solid
	1250 4250 1200 4200
Wire Notes Line style solid
	1200 4300 1250 4250
Wire Notes Line style solid
	1250 4350 1200 4300
Wire Notes Line style solid
	1200 4400 1250 4350
Wire Notes Line style solid
	1250 4450 1200 4400
Wire Notes Line style solid
	1200 4500 1250 4450
Wire Notes Line style solid
	1250 4550 1200 4500
Wire Notes Line style solid
	1250 4050 1250 4550
Wire Notes Line style solid
	1200 4700 1250 4650
Wire Notes Line style solid
	1250 4750 1200 4700
Wire Notes Line style solid
	1200 4800 1250 4750
Wire Notes Line style solid
	1250 4850 1200 4800
Wire Notes Line style solid
	1200 4900 1250 4850
Wire Notes Line style solid
	1250 4950 1200 4900
Wire Notes Line style solid
	1200 5000 1250 4950
Wire Notes Line style solid
	1250 5050 1200 5000
Wire Notes Line style solid
	1200 5100 1250 5050
Wire Notes Line style solid
	1250 5150 1200 5100
Wire Notes Line style solid
	1200 5200 1250 5150
Wire Notes Line style solid
	1250 5250 1200 5200
Wire Notes Line style solid
	1200 5300 1250 5250
Wire Notes Line style solid
	1250 5350 1200 5300
Wire Notes Line style solid
	1200 5400 1250 5350
Wire Notes Line style solid
	1250 5450 1200 5400
Wire Notes Line style solid
	1250 4650 1250 5450
Wire Notes Line style solid
	1200 5700 1250 5650
Wire Notes Line style solid
	1250 5750 1200 5700
Wire Notes Line style solid
	1200 5800 1250 5750
Wire Notes Line style solid
	1250 5850 1200 5800
Wire Notes Line style solid
	1200 5900 1250 5850
Wire Notes Line style solid
	1250 5950 1200 5900
Wire Notes Line style solid
	1200 6000 1250 5950
Wire Notes Line style solid
	1250 6050 1200 6000
Wire Notes Line style solid
	1200 6100 1250 6050
Wire Notes Line style solid
	1250 6150 1200 6100
Wire Notes Line style solid
	1250 5650 1250 6150
Wire Notes Line style solid
	1800 6100 1750 6150
Wire Notes Line style solid
	1750 6050 1800 6100
Wire Notes Line style solid
	1800 6000 1750 6050
Wire Notes Line style solid
	1750 5950 1800 6000
Wire Notes Line style solid
	1800 5900 1750 5950
Wire Notes Line style solid
	1750 5850 1800 5900
Wire Notes Line style solid
	1750 5850 1750 6150
Wire Notes Line style solid
	1750 5600 1800 5550
Wire Notes Line style solid
	1800 5650 1750 5600
Wire Notes Line style solid
	1750 5500 1800 5450
Wire Notes Line style solid
	1800 5550 1750 5500
Wire Notes Line style solid
	1750 5400 1800 5350
Wire Notes Line style solid
	1800 5450 1750 5400
Wire Notes Line style solid
	1750 5300 1800 5250
Wire Notes Line style solid
	1800 5350 1750 5300
Wire Notes Line style solid
	1750 5200 1800 5150
Wire Notes Line style solid
	1800 5250 1750 5200
Wire Notes Line style solid
	1750 5100 1800 5050
Wire Notes Line style solid
	1800 5150 1750 5100
Wire Notes Line style solid
	1750 5000 1800 4950
Wire Notes Line style solid
	1800 5050 1750 5000
Wire Notes Line style solid
	1750 4900 1800 4850
Wire Notes Line style solid
	1800 4950 1750 4900
Wire Notes Line style solid
	1800 4850 1800 5650
Wire Notes Line style solid
	1750 4600 1800 4550
Wire Notes Line style solid
	1800 4650 1750 4600
Wire Notes Line style solid
	1750 4500 1800 4450
Wire Notes Line style solid
	1800 4550 1750 4500
Wire Notes Line style solid
	1750 4400 1800 4350
Wire Notes Line style solid
	1800 4450 1750 4400
Wire Notes Line style solid
	1750 4300 1800 4250
Wire Notes Line style solid
	1800 4350 1750 4300
Wire Notes Line style solid
	1750 4200 1800 4150
Wire Notes Line style solid
	1800 4250 1750 4200
Wire Notes Line style solid
	1750 4100 1800 4050
Wire Notes Line style solid
	1800 4150 1750 4100
Wire Notes Line style solid
	1750 4000 1800 3950
Wire Notes Line style solid
	1800 4050 1750 4000
Wire Notes Line style solid
	1750 3900 1800 3850
Wire Notes Line style solid
	1800 3950 1750 3900
Wire Notes Line style solid
	1800 3850 1800 4650
Text Label 15900 3700 2    50   ~ 0
NC5
Text Label 15900 3800 2    50   ~ 0
GND
Text Label 15900 4000 2    50   ~ 0
SP_E
Text Label 15900 3900 2    50   ~ 0
SENSORDRV
Text Label 15900 4100 2    50   ~ 0
SP_F
Text Label 15900 4200 2    50   ~ 0
SP_G
Text Label 15900 4400 2    50   ~ 0
A-CENT
Text Label 15900 4300 2    50   ~ 0
V-CNG
Text Label 15900 4500 2    50   ~ 0
PULSE
Text Label 15900 4600 2    50   ~ 0
CW-CCW
Text Label 15900 4700 2    50   ~ 0
NC6
Text Label 15900 4800 2    50   ~ 0
GND
Text Label 15900 4900 2    50   ~ 0
Ready_Out
Text Label 15900 5000 2    50   ~ 0
D-Start
Text Label 15900 5100 2    50   ~ 0
D-Head
Text Label 15900 5200 2    50   ~ 0
T.Knock
Text Label 15900 5300 2    50   ~ 0
Centering
Text Label 15900 5600 2    50   ~ 0
Vac
Text Label 15900 5500 2    50   ~ 0
Head
Text Label 15900 5400 2    50   ~ 0
Rot
Text Label 15900 5700 2    50   ~ 0
+5V
Text Label 15900 5800 2    50   ~ 0
NC7
Text Label 15900 5900 2    50   ~ 0
M_ORG
Text Label 15900 6000 2    50   ~ 0
SP_9
Text Label 15900 6100 2    50   ~ 0
SP_10
Text Label 14450 6100 0    50   ~ 0
Ready_IN
Text Label 14450 6000 0    50   ~ 0
D-End
Text Label 14450 5900 0    50   ~ 0
Head-IN
Text Label 14450 5800 0    50   ~ 0
VCC_Sens
Text Label 14450 5700 0    50   ~ 0
90°
Text Label 14450 5600 0    50   ~ 0
+5V
Text Label 14450 5500 0    50   ~ 0
NC8
Text Label 14450 5400 0    50   ~ 0
+X
Text Label 14450 5300 0    50   ~ 0
-X
Text Label 14450 5200 0    50   ~ 0
+Y
Text Label 14450 5100 0    50   ~ 0
-Y
Text Label 14450 5000 0    50   ~ 0
Fast
Text Label 14450 4900 0    50   ~ 0
Teach
Text Label 14450 4800 0    50   ~ 0
T-VAC
Text Label 14450 4700 0    50   ~ 0
T-HEAD
Text Label 14450 4600 0    50   ~ 0
SELCT
Text Label 14450 4500 0    50   ~ 0
X-HM
Text Label 14450 4400 0    50   ~ 0
X-L4
Text Label 14450 4300 0    50   ~ 0
X-L3
Text Label 14450 4200 0    50   ~ 0
X-L2
Text Label 14450 4100 0    50   ~ 0
X-L1
Text Label 14450 4000 0    50   ~ 0
+5V
Text Label 14450 3900 0    50   ~ 0
GND
Text Label 14450 3800 0    50   ~ 0
X-CW
Text Label 14450 3700 0    50   ~ 0
X-CCW
Wire Wire Line
	14450 3700 14850 3700
Wire Wire Line
	14450 3800 14850 3800
Wire Wire Line
	14450 3900 14850 3900
Wire Wire Line
	14450 4000 14850 4000
Wire Wire Line
	14450 4100 14850 4100
Wire Wire Line
	14450 4200 14850 4200
Wire Wire Line
	14450 4300 14850 4300
Wire Wire Line
	14450 4400 14850 4400
Wire Wire Line
	14450 4500 14850 4500
Wire Wire Line
	14450 4600 14850 4600
Wire Wire Line
	14450 4700 14850 4700
Wire Wire Line
	14450 4800 14850 4800
Wire Wire Line
	14450 4900 14850 4900
Wire Wire Line
	14450 5000 14850 5000
Wire Wire Line
	14450 5100 14850 5100
Wire Wire Line
	14450 5200 14850 5200
Wire Wire Line
	14450 5300 14850 5300
Wire Wire Line
	14450 5400 14850 5400
Wire Wire Line
	14450 5500 14850 5500
Wire Wire Line
	14450 5600 14850 5600
Wire Wire Line
	14450 5700 14850 5700
Wire Wire Line
	14450 5800 14850 5800
Wire Wire Line
	14450 5900 14850 5900
Wire Wire Line
	14450 6000 14850 6000
Wire Wire Line
	14450 6100 14850 6100
Wire Wire Line
	15350 3700 15900 3700
Wire Wire Line
	15350 3800 15900 3800
Wire Wire Line
	15350 3900 15900 3900
Wire Wire Line
	15350 4000 15900 4000
Wire Wire Line
	15350 4100 15900 4100
Wire Wire Line
	15350 4200 15900 4200
Wire Wire Line
	15350 4300 15900 4300
Wire Wire Line
	15350 4400 15900 4400
Wire Wire Line
	15350 4500 15900 4500
Wire Wire Line
	15350 4600 15900 4600
Wire Wire Line
	15350 4700 15900 4700
Wire Wire Line
	15350 4800 15900 4800
Wire Wire Line
	15350 4900 15900 4900
Wire Wire Line
	15350 5000 15900 5000
Wire Wire Line
	15350 5100 15900 5100
Wire Wire Line
	15350 5200 15900 5200
Wire Wire Line
	15350 5300 15900 5300
Wire Wire Line
	15350 5400 15900 5400
Wire Wire Line
	15350 5500 15900 5500
Wire Wire Line
	15350 5600 15900 5600
Wire Wire Line
	15350 5700 15900 5700
Wire Wire Line
	15350 5800 15900 5800
Wire Wire Line
	15350 5900 15900 5900
Wire Wire Line
	15350 6000 15900 6000
Wire Wire Line
	15350 6100 15900 6100
Wire Notes Line style solid
	14850 3700 14800 3750
Wire Notes Line style solid
	14800 3650 14850 3700
Wire Notes Line style solid
	14850 3800 14800 3850
Wire Notes Line style solid
	14800 3750 14850 3800
Wire Notes Line style solid
	14800 3650 14800 3850
Wire Notes Line style solid
	14800 4100 14850 4050
Wire Notes Line style solid
	14850 4150 14800 4100
Wire Notes Line style solid
	14800 4200 14850 4150
Wire Notes Line style solid
	14850 4250 14800 4200
Wire Notes Line style solid
	14800 4300 14850 4250
Wire Notes Line style solid
	14850 4350 14800 4300
Wire Notes Line style solid
	14800 4400 14850 4350
Wire Notes Line style solid
	14850 4450 14800 4400
Wire Notes Line style solid
	14800 4500 14850 4450
Wire Notes Line style solid
	14850 4550 14800 4500
Wire Notes Line style solid
	14850 4050 14850 4550
Wire Notes Line style solid
	14800 4700 14850 4650
Wire Notes Line style solid
	14850 4750 14800 4700
Wire Notes Line style solid
	14800 4800 14850 4750
Wire Notes Line style solid
	14850 4850 14800 4800
Wire Notes Line style solid
	14800 4900 14850 4850
Wire Notes Line style solid
	14850 4950 14800 4900
Wire Notes Line style solid
	14800 5000 14850 4950
Wire Notes Line style solid
	14850 5050 14800 5000
Wire Notes Line style solid
	14800 5100 14850 5050
Wire Notes Line style solid
	14850 5150 14800 5100
Wire Notes Line style solid
	14800 5200 14850 5150
Wire Notes Line style solid
	14850 5250 14800 5200
Wire Notes Line style solid
	14800 5300 14850 5250
Wire Notes Line style solid
	14850 5350 14800 5300
Wire Notes Line style solid
	14800 5400 14850 5350
Wire Notes Line style solid
	14850 5450 14800 5400
Wire Notes Line style solid
	14850 4650 14850 5450
Wire Notes Line style solid
	14800 5700 14850 5650
Wire Notes Line style solid
	14850 5750 14800 5700
Wire Notes Line style solid
	14800 5800 14850 5750
Wire Notes Line style solid
	14850 5850 14800 5800
Wire Notes Line style solid
	14800 5900 14850 5850
Wire Notes Line style solid
	14850 5950 14800 5900
Wire Notes Line style solid
	14800 6000 14850 5950
Wire Notes Line style solid
	14850 6050 14800 6000
Wire Notes Line style solid
	14800 6100 14850 6050
Wire Notes Line style solid
	14850 6150 14800 6100
Wire Notes Line style solid
	14850 5650 14850 6150
Wire Notes Line style solid
	15400 6100 15350 6150
Wire Notes Line style solid
	15350 6050 15400 6100
Wire Notes Line style solid
	15400 6000 15350 6050
Wire Notes Line style solid
	15350 5950 15400 6000
Wire Notes Line style solid
	15400 5900 15350 5950
Wire Notes Line style solid
	15350 5850 15400 5900
Wire Notes Line style solid
	15350 5850 15350 6150
Wire Notes Line style solid
	15350 5600 15400 5550
Wire Notes Line style solid
	15400 5650 15350 5600
Wire Notes Line style solid
	15350 5500 15400 5450
Wire Notes Line style solid
	15400 5550 15350 5500
Wire Notes Line style solid
	15350 5400 15400 5350
Wire Notes Line style solid
	15400 5450 15350 5400
Wire Notes Line style solid
	15350 5300 15400 5250
Wire Notes Line style solid
	15400 5350 15350 5300
Wire Notes Line style solid
	15350 5200 15400 5150
Wire Notes Line style solid
	15400 5250 15350 5200
Wire Notes Line style solid
	15350 4900 15400 4850
Wire Notes Line style solid
	15400 4950 15350 4900
Wire Notes Line style solid
	15350 5000 15400 4950
Wire Notes Line style solid
	15400 5050 15350 5000
Wire Notes Line style solid
	15350 5100 15400 5050
Wire Notes Line style solid
	15400 5150 15350 5100
Wire Notes Line style solid
	15400 4850 15400 5650
Wire Notes Line style solid
	15350 3900 15400 3850
Wire Notes Line style solid
	15400 3950 15350 3900
Wire Notes Line style solid
	15350 4000 15400 3950
Wire Notes Line style solid
	15400 4050 15350 4000
Wire Notes Line style solid
	15350 4100 15400 4050
Wire Notes Line style solid
	15400 4150 15350 4100
Wire Notes Line style solid
	15350 4200 15400 4150
Wire Notes Line style solid
	15400 4250 15350 4200
Wire Notes Line style solid
	15350 4300 15400 4250
Wire Notes Line style solid
	15400 4350 15350 4300
Wire Notes Line style solid
	15350 4400 15400 4350
Wire Notes Line style solid
	15400 4450 15350 4400
Wire Notes Line style solid
	15350 4500 15400 4450
Wire Notes Line style solid
	15400 4550 15350 4500
Wire Notes Line style solid
	15350 4600 15400 4550
Wire Notes Line style solid
	15400 4650 15350 4600
Wire Notes Line style solid
	15400 3850 15400 4650
Text Label 4350 1750 1    50   ~ 0
PAT-L
Text Label 4250 1750 1    50   ~ 0
SPOT
Text Label 4550 1750 1    50   ~ 0
0.8_VAC
Text Label 4450 1750 1    50   ~ 0
0.8_HEAD
Text Label 4050 1750 1    50   ~ 0
STROBE
Text Label 4150 1750 1    50   ~ 0
DATA-4
Text Label 3850 1750 1    50   ~ 0
DATA-2
Text Label 3950 1750 1    50   ~ 0
DATA-1
Wire Wire Line
	4550 1300 4550 1750
Wire Wire Line
	4450 1300 4450 1750
Wire Wire Line
	4350 1300 4350 1750
Wire Wire Line
	4250 1300 4250 1750
Wire Wire Line
	4150 1300 4150 1750
Wire Wire Line
	4050 1300 4050 1750
Wire Wire Line
	3950 1300 3950 1750
Wire Wire Line
	3850 1300 3850 1750
Wire Notes Line style solid
	3850 1300 3900 1350
Wire Notes Line style solid
	3800 1350 3850 1300
Wire Notes Line style solid
	3950 1300 4000 1350
Wire Notes Line style solid
	3900 1350 3950 1300
Wire Notes Line style solid
	4050 1300 4100 1350
Wire Notes Line style solid
	4000 1350 4050 1300
Wire Notes Line style solid
	4150 1300 4200 1350
Wire Notes Line style solid
	4100 1350 4150 1300
Wire Notes Line style solid
	4250 1300 4300 1350
Wire Notes Line style solid
	4200 1350 4250 1300
Wire Notes Line style solid
	4350 1300 4400 1350
Wire Notes Line style solid
	4300 1350 4350 1300
Wire Notes Line style solid
	4450 1300 4500 1350
Wire Notes Line style solid
	4400 1350 4450 1300
Wire Notes Line style solid
	4550 1300 4600 1350
Wire Notes Line style solid
	4500 1350 4550 1300
Text Label 6050 1750 1    50   ~ 0
SP_A
Text Label 6150 1750 1    50   ~ 0
SP_B
Text Label 6250 1750 1    50   ~ 0
SP_C
Text Label 6350 1750 1    50   ~ 0
SP_D
Text Label 5850 1750 1    50   ~ 0
SUPPORTER
Text Label 5950 1750 1    50   ~ 0
LOCATOR
Text Label 5650 1750 1    50   ~ 0
STOPPER
Wire Wire Line
	5950 1750 5950 1300
Wire Wire Line
	5850 1300 5850 1750
Wire Wire Line
	5750 1750 5750 1300
Wire Wire Line
	6250 1300 6250 1750
Wire Wire Line
	6150 1300 6150 1750
Wire Wire Line
	6050 1300 6050 1750
Wire Wire Line
	5650 1300 5650 1750
Text Label 5750 1750 1    50   ~ 0
CVY-M
Wire Notes Line style solid
	5650 1300 5700 1350
Wire Notes Line style solid
	5600 1350 5650 1300
Wire Notes Line style solid
	5750 1300 5800 1350
Wire Notes Line style solid
	5700 1350 5750 1300
Wire Notes Line style solid
	5850 1300 5900 1350
Wire Notes Line style solid
	5800 1350 5850 1300
Wire Notes Line style solid
	5950 1300 6000 1350
Wire Notes Line style solid
	5900 1350 5950 1300
Wire Notes Line style solid
	6050 1300 6100 1350
Wire Notes Line style solid
	6000 1350 6050 1300
Wire Notes Line style solid
	6150 1300 6200 1350
Wire Notes Line style solid
	6100 1350 6150 1300
Wire Notes Line style solid
	6250 1300 6300 1350
Wire Notes Line style solid
	6200 1350 6250 1300
Wire Notes Line style solid
	6350 1300 6400 1350
Wire Notes Line style solid
	6300 1350 6350 1300
Text Label 7450 1750 1    50   ~ 0
Y_CCW
Text Label 7550 1750 1    50   ~ 0
Y_CW
Wire Wire Line
	7450 1750 7450 1300
Wire Wire Line
	7550 1750 7550 1300
Wire Notes Line style solid
	7450 1300 7500 1350
Wire Notes Line style solid
	7400 1350 7450 1300
Wire Notes Line style solid
	7550 1300 7600 1350
Wire Notes Line style solid
	7500 1350 7550 1300
Text Label 11650 1750 1    50   ~ 0
SP_E
Text Label 11750 1750 1    50   ~ 0
SENSORDRV
Text Label 11550 1750 1    50   ~ 0
SP_F
Text Label 11450 1750 1    50   ~ 0
SP_G
Text Label 11150 1750 1    50   ~ 0
A-CENT
Text Label 11050 1750 1    50   ~ 0
V-CNG
Text Label 11250 1750 1    50   ~ 0
PULSE
Text Label 11350 1750 1    50   ~ 0
CW-CCW
Wire Wire Line
	11750 1200 11750 1750
Wire Wire Line
	11650 1200 11650 1750
Wire Wire Line
	11550 1200 11550 1750
Wire Wire Line
	11450 1200 11450 1750
Wire Wire Line
	11350 1200 11350 1750
Wire Wire Line
	11250 1200 11250 1750
Wire Wire Line
	11150 1200 11150 1750
Wire Wire Line
	11050 1200 11050 1750
Wire Notes Line style solid
	11750 1200 11800 1250
Wire Notes Line style solid
	11700 1250 11750 1200
Wire Notes Line style solid
	11650 1200 11700 1250
Wire Notes Line style solid
	11600 1250 11650 1200
Wire Notes Line style solid
	11550 1200 11600 1250
Wire Notes Line style solid
	11500 1250 11550 1200
Wire Notes Line style solid
	11450 1200 11500 1250
Wire Notes Line style solid
	11400 1250 11450 1200
Wire Notes Line style solid
	11350 1200 11400 1250
Wire Notes Line style solid
	11300 1250 11350 1200
Wire Notes Line style solid
	11250 1200 11300 1250
Wire Notes Line style solid
	11200 1250 11250 1200
Wire Notes Line style solid
	11150 1200 11200 1250
Wire Notes Line style solid
	11100 1250 11150 1200
Wire Notes Line style solid
	11050 1200 11100 1250
Wire Notes Line style solid
	11000 1250 11050 1200
Text Label 9850 1750 1    50   ~ 0
Ready_Out
Text Label 9950 1750 1    50   ~ 0
D-Start
Text Label 9650 1750 1    50   ~ 0
D-Head
Text Label 9750 1750 1    50   ~ 0
T.Knock
Text Label 9250 1750 1    50   ~ 0
Centering
Text Label 9550 1750 1    50   ~ 0
Vac
Text Label 9450 1750 1    50   ~ 0
Head
Text Label 9350 1750 1    50   ~ 0
Rot
Wire Wire Line
	9950 1200 9950 1750
Wire Wire Line
	9850 1200 9850 1750
Wire Wire Line
	9750 1200 9750 1750
Wire Wire Line
	9650 1200 9650 1750
Wire Wire Line
	9550 1200 9550 1750
Wire Wire Line
	9450 1200 9450 1750
Wire Wire Line
	9350 1200 9350 1750
Wire Wire Line
	9250 1200 9250 1750
Wire Notes Line style solid
	9250 1200 9300 1250
Wire Notes Line style solid
	9200 1250 9250 1200
Wire Notes Line style solid
	9350 1200 9400 1250
Wire Notes Line style solid
	9300 1250 9350 1200
Wire Notes Line style solid
	9450 1200 9500 1250
Wire Notes Line style solid
	9400 1250 9450 1200
Wire Notes Line style solid
	9550 1200 9600 1250
Wire Notes Line style solid
	9500 1250 9550 1200
Wire Notes Line style solid
	9650 1200 9700 1250
Wire Notes Line style solid
	9600 1250 9650 1200
Wire Notes Line style solid
	9950 1200 10000 1250
Wire Notes Line style solid
	9900 1250 9950 1200
Wire Notes Line style solid
	9850 1200 9900 1250
Wire Notes Line style solid
	9800 1250 9850 1200
Wire Notes Line style solid
	9750 1200 9800 1250
Wire Notes Line style solid
	9700 1250 9750 1200
Text Label 8150 1750 1    50   ~ 0
X-CW
Text Label 8050 1750 1    50   ~ 0
X-CCW
Wire Wire Line
	8050 1750 8050 1350
Wire Wire Line
	8150 1750 8150 1350
Wire Notes Line style solid
	8050 1350 8100 1400
Wire Notes Line style solid
	8000 1400 8050 1350
Wire Notes Line style solid
	8150 1350 8200 1400
Wire Notes Line style solid
	8100 1400 8150 1350
Text Label 5750 9600 3    50   ~ 0
SP_8
Wire Wire Line
	5750 9600 5750 10050
Text Label 5850 9600 3    50   ~ 0
PIN_UP
Text Label 5950 9600 3    50   ~ 0
PCB_OUT
Text Label 6050 9600 3    50   ~ 0
PCB_IN
Text Label 6250 9600 3    50   ~ 0
CCW_SW
Text Label 6150 9600 3    50   ~ 0
PCB_DET
Text Label 6350 9600 3    50   ~ 0
CW_SW
Text Label 6450 9600 3    50   ~ 0
M_CENTER
Text Label 6950 9600 3    50   ~ 0
Y_L1
Text Label 6850 9600 3    50   ~ 0
Y_L2
Text Label 6750 9600 3    50   ~ 0
Y_L3
Text Label 6650 9600 3    50   ~ 0
Y_L4
Text Label 6550 9600 3    50   ~ 0
Y_HM
Wire Wire Line
	6850 9600 6850 10050
Wire Wire Line
	6750 9600 6750 10050
Wire Wire Line
	6650 9600 6650 10050
Wire Wire Line
	6550 9600 6550 10050
Wire Wire Line
	6450 9600 6450 10050
Wire Wire Line
	6350 9600 6350 10050
Wire Wire Line
	6250 9600 6250 10050
Wire Wire Line
	6150 9600 6150 10050
Wire Wire Line
	6050 9600 6050 10050
Wire Wire Line
	5950 9600 5950 10050
Wire Wire Line
	5850 9600 5850 10050
Wire Notes Line style solid
	6950 10000 7000 10050
Wire Notes Line style solid
	6900 10050 6950 10000
Wire Notes Line style solid
	6850 10000 6900 10050
Wire Notes Line style solid
	6800 10050 6850 10000
Wire Notes Line style solid
	6750 10000 6800 10050
Wire Notes Line style solid
	6700 10050 6750 10000
Wire Notes Line style solid
	6650 10000 6700 10050
Wire Notes Line style solid
	6600 10050 6650 10000
Wire Notes Line style solid
	6550 10000 6600 10050
Wire Notes Line style solid
	6500 10050 6550 10000
Wire Notes Line style solid
	6450 10000 6500 10050
Wire Notes Line style solid
	6400 10050 6450 10000
Wire Notes Line style solid
	6350 10000 6400 10050
Wire Notes Line style solid
	6300 10050 6350 10000
Wire Notes Line style solid
	6250 10000 6300 10050
Wire Notes Line style solid
	6200 10050 6250 10000
Wire Notes Line style solid
	6150 10000 6200 10050
Wire Notes Line style solid
	6100 10050 6150 10000
Wire Notes Line style solid
	6050 10000 6100 10050
Wire Notes Line style solid
	6000 10050 6050 10000
Wire Notes Line style solid
	5950 10000 6000 10050
Wire Notes Line style solid
	5900 10050 5950 10000
Wire Notes Line style solid
	5850 10000 5900 10050
Wire Notes Line style solid
	5800 10050 5850 10000
Wire Notes Line style solid
	5750 10000 5800 10050
Wire Notes Line style solid
	5700 10050 5750 10000
Text Label 5250 9600 3    50   ~ 0
SP_4
Text Label 5350 9600 3    50   ~ 0
SP_5
Text Label 5450 9600 3    50   ~ 0
BAD_MARK
Text Label 5550 9600 3    50   ~ 0
SP_6
Text Label 5650 9600 3    50   ~ 0
SP_7
Wire Wire Line
	5650 9600 5650 10050
Wire Wire Line
	5550 9600 5550 10050
Wire Wire Line
	5450 10050 5450 9600
Wire Wire Line
	5350 9600 5350 10050
Wire Wire Line
	5250 10050 5250 9600
Wire Notes Line style solid
	5650 10000 5700 10050
Wire Notes Line style solid
	5600 10050 5650 10000
Wire Notes Line style solid
	5550 10000 5600 10050
Wire Notes Line style solid
	5500 10050 5550 10000
Wire Notes Line style solid
	5450 10000 5500 10050
Wire Notes Line style solid
	5400 10050 5450 10000
Wire Notes Line style solid
	5350 10000 5400 10050
Wire Notes Line style solid
	5300 10050 5350 10000
Wire Notes Line style solid
	5250 10000 5300 10050
Wire Notes Line style solid
	5200 10050 5250 10000
Text Label 4950 9600 3    50   ~ 0
SP_1
Text Label 5050 9600 3    50   ~ 0
SP_2
Text Label 5150 9600 3    50   ~ 0
SP_3
Wire Wire Line
	4950 10050 4950 9600
Wire Wire Line
	5050 9600 5050 10050
Wire Wire Line
	5150 10050 5150 9600
Wire Notes Line style solid
	5150 10000 5200 10050
Wire Notes Line style solid
	5100 10050 5150 10000
Wire Notes Line style solid
	5050 10000 5100 10050
Wire Notes Line style solid
	5000 10050 5050 10000
Wire Notes Line style solid
	4950 10000 5000 10050
Wire Notes Line style solid
	4900 10050 4950 10000
Text Label 7550 9600 3    50   ~ 0
M_ORG
Text Label 7650 9600 3    50   ~ 0
SP_9
Text Label 7750 9600 3    50   ~ 0
SP_10
Wire Wire Line
	7550 10000 7550 9600
Wire Wire Line
	7650 10000 7650 9600
Wire Wire Line
	7750 10000 7750 9600
Wire Notes Line style solid
	7750 9950 7800 10000
Wire Notes Line style solid
	7700 10000 7750 9950
Wire Notes Line style solid
	7650 9950 7700 10000
Wire Notes Line style solid
	7600 10000 7650 9950
Wire Notes Line style solid
	7550 9950 7600 10000
Wire Notes Line style solid
	7500 10000 7550 9950
Text Label 7850 9600 3    50   ~ 0
Ready_IN
Text Label 7950 9600 3    50   ~ 0
D-End
Text Label 8050 9600 3    50   ~ 0
Head-IN
Text Label 8150 9600 3    50   ~ 0
VCC_Sens
Text Label 8250 9600 3    50   ~ 0
90°
Text Label 8350 9600 3    50   ~ 0
+X
Text Label 8450 9600 3    50   ~ 0
-X
Text Label 8550 9600 3    50   ~ 0
+Y
Text Label 8650 9600 3    50   ~ 0
-Y
Text Label 8750 9600 3    50   ~ 0
Fast
Text Label 8850 9600 3    50   ~ 0
Teach
Text Label 8950 9600 3    50   ~ 0
T-VAC
Text Label 9050 9600 3    50   ~ 0
T-HEAD
Text Label 9150 9600 3    50   ~ 0
X-HM
Text Label 9250 9600 3    50   ~ 0
X-L4
Text Label 9350 9600 3    50   ~ 0
X-L3
Text Label 9450 9600 3    50   ~ 0
X-L2
Text Label 9550 9600 3    50   ~ 0
X-L1
Wire Wire Line
	9550 9600 9550 10000
Wire Wire Line
	9450 9600 9450 10000
Wire Wire Line
	9350 9600 9350 10000
Wire Wire Line
	9250 9600 9250 10000
Wire Wire Line
	9150 9600 9150 10000
Wire Wire Line
	9050 9600 9050 10000
Wire Wire Line
	8950 9600 8950 10000
Wire Wire Line
	8850 9600 8850 10000
Wire Wire Line
	8750 9600 8750 10000
Wire Wire Line
	8650 9600 8650 10000
Wire Wire Line
	8550 9600 8550 10000
Wire Wire Line
	8450 9600 8450 10000
Wire Wire Line
	8350 9600 8350 10000
Wire Wire Line
	8250 9600 8250 10000
Wire Wire Line
	8150 9600 8150 10000
Wire Wire Line
	8050 9600 8050 10000
Wire Wire Line
	7950 9600 7950 10000
Wire Wire Line
	7850 9600 7850 10000
Wire Notes Line style solid
	9550 9950 9600 10000
Wire Notes Line style solid
	9500 10000 9550 9950
Wire Notes Line style solid
	9450 9950 9500 10000
Wire Notes Line style solid
	9400 10000 9450 9950
Wire Notes Line style solid
	9350 9950 9400 10000
Wire Notes Line style solid
	9300 10000 9350 9950
Wire Notes Line style solid
	9250 9950 9300 10000
Wire Notes Line style solid
	9200 10000 9250 9950
Wire Notes Line style solid
	9150 9950 9200 10000
Wire Notes Line style solid
	9100 10000 9150 9950
Wire Notes Line style solid
	9050 9950 9100 10000
Wire Notes Line style solid
	9000 10000 9050 9950
Wire Notes Line style solid
	8950 9950 9000 10000
Wire Notes Line style solid
	8900 10000 8950 9950
Wire Notes Line style solid
	8850 9950 8900 10000
Wire Notes Line style solid
	8800 10000 8850 9950
Wire Notes Line style solid
	8750 9950 8800 10000
Wire Notes Line style solid
	8700 10000 8750 9950
Wire Notes Line style solid
	8650 9950 8700 10000
Wire Notes Line style solid
	8600 10000 8650 9950
Wire Notes Line style solid
	8550 9950 8600 10000
Wire Notes Line style solid
	8500 10000 8550 9950
Wire Notes Line style solid
	8450 9950 8500 10000
Wire Notes Line style solid
	8400 10000 8450 9950
Wire Notes Line style solid
	8350 9950 8400 10000
Wire Notes Line style solid
	8300 10000 8350 9950
Wire Notes Line style solid
	8250 9950 8300 10000
Wire Notes Line style solid
	8200 10000 8250 9950
Wire Notes Line style solid
	8150 9950 8200 10000
Wire Notes Line style solid
	8100 10000 8150 9950
Wire Notes Line style solid
	8050 9950 8100 10000
Wire Notes Line style solid
	8000 10000 8050 9950
Wire Notes Line style solid
	7950 9950 8000 10000
Wire Notes Line style solid
	7900 10000 7950 9950
Wire Notes Line style solid
	7850 9950 7900 10000
Wire Notes Line style solid
	7800 10000 7850 9950
Text Notes 7900 1250 0    50   ~ 0
36 OUTPUT
Text Notes 7050 10200 0    50   ~ 0
42 INPUT
$Comp
L MCU_ST_STM32F4:STM32F407ZGTx U4
U 1 1 5CF790FD
P 7750 5750
F 0 "U4" V 7850 5800 100 0000 R CNN
F 1 "STM32F407ZGTx" V 7750 6000 50  0000 R CNN
F 2 "Housings_QFP:LQFP-144_20x20mm_Pitch0.5mm" H 6750 2350 50  0001 R CNN
F 3 "http://www.st.com/st-web-ui/static/active/en/resource/technical/document/datasheet/DM00037051.pdf" H 7750 5750 50  0001 C CNN
	1    7750 5750
	0    -1   -1   0   
$EndComp
$Comp
L 74xx:74HC240 U1
U 1 1 5D1D340E
P 4350 2250
F 0 "U1" V 4500 1900 50  0000 R CNN
F 1 "74HCT240PW" V 4700 2050 50  0000 R CNN
F 2 "Housings_SSOP:SSOP-20_4.4x6.5mm_Pitch0.65mm" H 4350 2250 50  0001 C CNN
F 3 "http://www.nxp.com/documents/data_sheet/74HC_HCT240.pdf" H 4350 2250 50  0001 C CNN
	1    4350 2250
	0    -1   -1   0   
$EndComp
$Comp
L 74xx:74HC240 U6
U 1 1 5D232DCA
P 11550 2250
F 0 "U6" V 11750 1900 50  0000 R CNN
F 1 "74HCT240PW" V 11900 2050 50  0000 R CNN
F 2 "Housings_SSOP:SSOP-20_4.4x6.5mm_Pitch0.65mm" H 11550 2250 50  0001 C CNN
F 3 "http://www.nxp.com/documents/data_sheet/74HC_HCT240.pdf" H 11550 2250 50  0001 C CNN
	1    11550 2250
	0    -1   -1   0   
$EndComp
$Comp
L 74xx:74HC240 U3
U 1 1 5D29268F
P 7950 2250
F 0 "U3" V 8100 1900 50  0000 R CNN
F 1 "74HCT240PW" V 8300 2050 50  0000 R CNN
F 2 "Housings_SSOP:SSOP-20_4.4x6.5mm_Pitch0.65mm" H 7950 2250 50  0001 C CNN
F 3 "http://www.nxp.com/documents/data_sheet/74HC_HCT240.pdf" H 7950 2250 50  0001 C CNN
	1    7950 2250
	0    -1   -1   0   
$EndComp
$Comp
L 74xx:74HC240 U5
U 1 1 5D2B2516
P 9750 2250
F 0 "U5" V 9900 1900 50  0000 R CNN
F 1 "74HCT240PW" V 10100 2050 50  0000 R CNN
F 2 "Housings_SSOP:SSOP-20_4.4x6.5mm_Pitch0.65mm" H 9750 2250 50  0001 C CNN
F 3 "http://www.nxp.com/documents/data_sheet/74HC_HCT240.pdf" H 9750 2250 50  0001 C CNN
	1    9750 2250
	0    -1   -1   0   
$EndComp
$Comp
L 74xx:74HC240 U2
U 1 1 5D2F197E
P 6150 2250
F 0 "U2" V 6300 1900 50  0000 R CNN
F 1 "74HCT240PW" V 6500 2050 50  0000 R CNN
F 2 "Housings_SSOP:SSOP-20_4.4x6.5mm_Pitch0.65mm" H 6150 2250 50  0001 C CNN
F 3 "http://www.nxp.com/documents/data_sheet/74HC_HCT240.pdf" H 6150 2250 50  0001 C CNN
	1    6150 2250
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_Pack08 RN6
U 1 1 5D4AA173
P 6650 9400
F 0 "RN6" H 7038 9446 50  0000 L CNN
F 1 "EXB-2HV102JV" H 7038 9355 50  0001 L CNN
F 2 "Resistors_SMD:R_Array_Convex_8x0602" V 7125 9400 50  0001 C CNN
F 3 "~" H 6650 9400 50  0001 C CNN
	1    6650 9400
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Pack08 RN11
U 1 1 5D4AA3D0
P 9450 8350
F 0 "RN11" H 9550 8700 50  0000 R CNN
F 1 "EXB-2HV202JV" H 9700 8600 50  0000 R CNN
F 2 "Resistors_SMD:R_Array_Convex_8x0602" V 9925 8350 50  0001 C CNN
F 3 "~" H 9450 8350 50  0001 C CNN
	1    9450 8350
	-1   0    0    -1  
$EndComp
$Comp
L Device:R_Pack08 RN8
U 1 1 5D58CFED
P 7850 9400
F 0 "RN8" H 7370 9446 50  0000 R CNN
F 1 "EXB-2HV102JV" H 7370 9355 50  0000 R CNN
F 2 "Resistors_SMD:R_Array_Convex_8x0602" V 8325 9400 50  0001 C CNN
F 3 "~" H 7850 9400 50  0001 C CNN
	1    7850 9400
	-1   0    0    -1  
$EndComp
$Comp
L Device:R_Pack08 RN10
U 1 1 5D58D0D6
P 8650 9400
F 0 "RN10" H 8170 9446 50  0000 R CNN
F 1 "EXB-2HV102JV" H 8170 9355 50  0000 R CNN
F 2 "Resistors_SMD:R_Array_Convex_8x0602" V 9125 9400 50  0001 C CNN
F 3 "~" H 8650 9400 50  0001 C CNN
	1    8650 9400
	-1   0    0    -1  
$EndComp
$Comp
L Device:R_Pack08 RN12
U 1 1 5D58D11C
P 9450 9400
F 0 "RN12" H 8970 9446 50  0000 R CNN
F 1 "EXB-2HV102JV" H 8950 9350 50  0000 R CNN
F 2 "Resistors_SMD:R_Array_Convex_8x0602" V 9925 9400 50  0001 C CNN
F 3 "~" H 9450 9400 50  0001 C CNN
	1    9450 9400
	-1   0    0    -1  
$EndComp
$Comp
L Device:R_Pack08 RN4
U 1 1 5D58D39E
P 5850 9400
F 0 "RN4" H 5700 9700 50  0000 L CNN
F 1 "EXB-2HV102JV" H 5650 9400 50  0001 L CNN
F 2 "Resistors_SMD:R_Array_Convex_8x0602" V 6325 9400 50  0001 C CNN
F 3 "~" H 5850 9400 50  0001 C CNN
	1    5850 9400
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Pack08 RN2
U 1 1 5D58D3FC
P 5050 9400
F 0 "RN2" H 4400 9400 50  0000 L CNN
F 1 "EXB-2HV102JV" H 4050 9250 50  0000 L CNN
F 2 "Resistors_SMD:R_Array_Convex_8x0602" V 5525 9400 50  0001 C CNN
F 3 "~" H 5050 9400 50  0001 C CNN
	1    5050 9400
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Pack08 RN9
U 1 1 5D58D521
P 8650 8350
F 0 "RN9" H 8650 8700 50  0000 R CNN
F 1 "EXB-2HV202JV" H 8850 8600 50  0000 R CNN
F 2 "Resistors_SMD:R_Array_Convex_8x0602" V 9125 8350 50  0001 C CNN
F 3 "~" H 8650 8350 50  0001 C CNN
	1    8650 8350
	-1   0    0    -1  
$EndComp
$Comp
L Device:R_Pack08 RN7
U 1 1 5D58D56D
P 7850 8350
F 0 "RN7" H 7900 8700 50  0000 R CNN
F 1 "EXB-2HV202JV" H 8100 8600 50  0000 R CNN
F 2 "Resistors_SMD:R_Array_Convex_8x0602" V 8325 8350 50  0001 C CNN
F 3 "~" H 7850 8350 50  0001 C CNN
	1    7850 8350
	-1   0    0    -1  
$EndComp
$Comp
L Device:R_Pack08 RN5
U 1 1 5D58D5CB
P 6650 8350
F 0 "RN5" H 6500 8700 50  0000 L CNN
F 1 "EXB-2HV202JV" H 6350 8600 50  0000 L CNN
F 2 "Resistors_SMD:R_Array_Convex_8x0602" V 7125 8350 50  0001 C CNN
F 3 "~" H 6650 8350 50  0001 C CNN
	1    6650 8350
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Pack08 RN3
U 1 1 5D58D65D
P 5850 8350
F 0 "RN3" H 5750 8700 50  0000 L CNN
F 1 "EXB-2HV202JV" H 5550 8600 50  0000 L CNN
F 2 "Resistors_SMD:R_Array_Convex_8x0602" V 6325 8350 50  0001 C CNN
F 3 "~" H 5850 8350 50  0001 C CNN
	1    5850 8350
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Pack08 RN1
U 1 1 5D58D6AF
P 5050 8350
F 0 "RN1" H 4900 8700 50  0000 L CNN
F 1 "EXB-2HV202JV" H 4700 8600 50  0000 L CNN
F 2 "Resistors_SMD:R_Array_Convex_8x0602" V 5525 8350 50  0001 C CNN
F 3 "~" H 5050 8350 50  0001 C CNN
	1    5050 8350
	1    0    0    -1  
$EndComp
Wire Wire Line
	4750 8550 4750 9200
Wire Wire Line
	4850 8550 4850 9200
Wire Wire Line
	4950 8550 4950 9200
Wire Wire Line
	6950 8550 6950 9200
Wire Wire Line
	6850 8550 6850 9200
Wire Wire Line
	6750 8550 6750 9200
Wire Wire Line
	6650 8550 6650 9200
Wire Wire Line
	6550 8550 6550 9200
Wire Wire Line
	6450 8550 6450 9200
Wire Wire Line
	6350 8550 6350 9200
Wire Wire Line
	6250 8550 6250 9200
Wire Wire Line
	6150 8550 6150 9200
Wire Wire Line
	6050 8550 6050 9200
Wire Wire Line
	5950 8550 5950 9200
Wire Wire Line
	5850 8550 5850 9000
Wire Wire Line
	5750 8550 5750 9000
Wire Wire Line
	5650 8550 5650 9200
Wire Wire Line
	5550 8550 5550 9200
Wire Wire Line
	5450 8550 5450 9200
Wire Wire Line
	5350 8550 5350 9200
Wire Wire Line
	5250 8550 5250 9200
Wire Wire Line
	5150 8550 5150 9200
Wire Wire Line
	5050 8550 5050 9200
Wire Wire Line
	9850 8550 9850 9200
Wire Wire Line
	9750 8550 9750 9200
Wire Wire Line
	9650 8550 9650 9200
Wire Wire Line
	9550 8550 9550 9200
Wire Wire Line
	9450 8550 9450 9200
Wire Wire Line
	9350 8550 9350 9200
Wire Wire Line
	9250 8550 9250 9200
Wire Wire Line
	9150 8550 9150 9200
Wire Wire Line
	9050 8550 9050 9200
Wire Wire Line
	8950 8550 8950 9200
Wire Wire Line
	8850 8550 8850 9200
Wire Wire Line
	8750 8550 8750 9200
Wire Wire Line
	8650 8550 8650 9200
Wire Wire Line
	8550 8550 8550 9200
Wire Wire Line
	8450 8550 8450 9200
Wire Wire Line
	8350 8550 8350 9200
Wire Wire Line
	8250 8550 8250 9200
Wire Wire Line
	8150 8550 8150 9200
Wire Wire Line
	8050 8550 8050 9200
Wire Wire Line
	7950 8550 7950 9200
Wire Wire Line
	7850 8550 7850 9200
Wire Wire Line
	7750 8550 7750 9200
Wire Wire Line
	7650 8550 7650 9200
Wire Wire Line
	7550 8550 7550 9200
Wire Wire Line
	6950 9600 6950 10050
$Comp
L power:GND #PWR02
U 1 1 5D978D27
P 7250 8150
F 0 "#PWR02" H 7250 7900 50  0001 C CNN
F 1 "GND" H 7350 8050 50  0000 C CNN
F 2 "" H 7250 8150 50  0001 C CNN
F 3 "" H 7250 8150 50  0001 C CNN
	1    7250 8150
	1    0    0    -1  
$EndComp
Wire Wire Line
	6950 8150 7250 8150
Connection ~ 7250 8150
Wire Wire Line
	7250 8150 7550 8150
$Comp
L power:+3.3V #PWR01
U 1 1 5D99ED22
P 3950 4900
F 0 "#PWR01" H 3950 4750 50  0001 C CNN
F 1 "+3.3V" H 3950 5050 50  0000 C CNN
F 2 "" H 3950 4900 50  0001 C CNN
F 3 "" H 3950 4900 50  0001 C CNN
	1    3950 4900
	1    0    0    -1  
$EndComp
Wire Wire Line
	3950 5050 4150 5050
Wire Wire Line
	3950 5150 4150 5150
Wire Wire Line
	3950 5250 4150 5250
Wire Wire Line
	3950 5350 4150 5350
Wire Wire Line
	3950 5450 4150 5450
Wire Wire Line
	3950 5550 4150 5550
Wire Wire Line
	3950 5650 4150 5650
Wire Wire Line
	3950 5750 4150 5750
Wire Wire Line
	3950 5850 4150 5850
Wire Wire Line
	3950 5950 4150 5950
Wire Wire Line
	3950 6050 4150 6050
Wire Wire Line
	3950 6150 4150 6150
Wire Wire Line
	3950 6250 4150 6250
Wire Wire Line
	3950 6350 4150 6350
Wire Wire Line
	3950 5150 3950 5050
Wire Wire Line
	3950 5250 3950 5150
Wire Wire Line
	3950 5350 3950 5250
Wire Wire Line
	3950 5450 3950 5350
Wire Wire Line
	3950 5550 3950 5450
Wire Wire Line
	3950 5650 3950 5550
Wire Wire Line
	3950 5750 3950 5650
Wire Wire Line
	3950 5850 3950 5750
Wire Wire Line
	3950 5950 3950 5850
Wire Wire Line
	3950 6050 3950 5950
Wire Wire Line
	3950 6150 3950 6050
Wire Wire Line
	3950 6250 3950 6150
Wire Wire Line
	3950 4900 3950 5050
Connection ~ 3950 5050
Connection ~ 3950 5150
Connection ~ 3950 5250
Connection ~ 3950 5350
Connection ~ 3950 5450
Connection ~ 3950 5550
Connection ~ 3950 5650
Connection ~ 3950 5750
Connection ~ 3950 5850
Connection ~ 3950 5950
Connection ~ 3950 6050
Connection ~ 3950 6150
$Comp
L power:GND #PWR03
U 1 1 5DADD4BD
P 11550 6250
F 0 "#PWR03" H 11550 6000 50  0001 C CNN
F 1 "GND" H 11555 6077 50  0000 C CNN
F 2 "" H 11550 6250 50  0001 C CNN
F 3 "" H 11550 6250 50  0001 C CNN
	1    11550 6250
	1    0    0    -1  
$EndComp
Wire Wire Line
	11350 6150 11550 6150
Wire Wire Line
	11550 6150 11550 6250
Wire Wire Line
	11350 6050 11550 6050
Wire Wire Line
	11350 5250 11550 5250
Wire Wire Line
	11550 5250 11550 5350
Wire Wire Line
	11350 5350 11550 5350
Wire Wire Line
	11550 5350 11550 5450
Connection ~ 11550 5350
Wire Wire Line
	11550 6050 11550 6150
Wire Wire Line
	11350 5450 11550 5450
Wire Wire Line
	11550 5450 11550 5550
Connection ~ 11550 5450
Wire Wire Line
	11350 5550 11550 5550
Wire Wire Line
	11350 5650 11550 5650
Wire Wire Line
	11350 5750 11550 5750
Wire Wire Line
	11350 5850 11550 5850
Wire Wire Line
	11350 5950 11550 5950
Connection ~ 11550 5550
Wire Wire Line
	11550 5550 11550 5650
Connection ~ 11550 5650
Connection ~ 11550 5750
Connection ~ 11550 5850
Connection ~ 11550 5950
Connection ~ 11550 6050
Connection ~ 11550 6150
Wire Wire Line
	11550 5650 11550 5750
Wire Wire Line
	11550 5750 11550 5850
Wire Wire Line
	11550 5850 11550 5950
Wire Wire Line
	11550 5950 11550 6050
$Comp
L Connector_Generic:Conn_02x03_Counter_Clockwise J99
U 1 1 5DC09DB9
P 12400 7450
F 0 "J99" H 12450 7700 100 0000 C CNN
F 1 "Conn_02x03_Counter_Clockwise" H 12450 7676 50  0001 C CNN
F 2 "Connectors:Tag-Connect_TC2030-IDC-NL" H 12400 7450 50  0001 C CNN
F 3 "~" H 12400 7450 50  0001 C CNN
	1    12400 7450
	1    0    0    -1  
$EndComp
$Comp
L Interface_Ethernet:KSZ8081RNA U7
U 1 1 5DFEC5CA
P 1650 8550
F 0 "U7" V 1800 8700 100 0000 R CNN
F 1 "KSZ8081RNA" V 1950 9000 50  0000 R CNN
F 2 "Housings_DFN_QFN:QFN-24-1EP_4x4mm_Pitch0.5mm" H 3050 7550 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/00002199A.pdf" H -600 8750 50  0001 C CNN
	1    1650 8550
	0    1    1    0   
$EndComp
$Comp
L Connector:RJ45_Amphenol_RJMG1BD3B8K1ANR J2
U 1 1 5DFECA06
P 1500 10450
F 0 "J2" H 1800 10050 100 0000 C CNN
F 1 "RJ45_Amphenol_RJMG1BD3B8K1ANR" H 1500 11050 50  0000 C CNN
F 2 "Connectors_RJ:RJ45_Amphenol_RJMG1BD3B8K1ANR" H 1500 11150 50  0001 C CNN
F 3 "https://www.amphenolcanada.com/ProductSearch/Drawings/AC/RJMG1BD3B8K1ANR.PDF" H 1500 11250 50  0001 C CNN
	1    1500 10450
	-1   0    0    1   
$EndComp
$Comp
L Device:Ferrite_Bead_Small FB1
U 1 1 5DFECF1F
P 900 9700
F 0 "FB1" V 800 9700 50  0000 C CNN
F 1 "Ferrite_Bead_Small" V 754 9700 50  0001 C CNN
F 2 "Inductors_SMD:L_0805" V 830 9700 50  0001 C CNN
F 3 "~" H 900 9700 50  0001 C CNN
	1    900  9700
	0    1    1    0   
$EndComp
Wire Wire Line
	600  10050 600  9700
Wire Wire Line
	600  9700 800  9700
Wire Wire Line
	1000 9700 1500 9700
Wire Wire Line
	1500 9700 1500 9750
$Comp
L power:GND #PWR0101
U 1 1 5E047B2D
P 1500 9700
F 0 "#PWR0101" H 1500 9450 50  0001 C CNN
F 1 "GND" H 1500 9550 50  0000 C CNN
F 2 "" H 1500 9700 50  0001 C CNN
F 3 "" H 1500 9700 50  0001 C CNN
	1    1500 9700
	-1   0    0    1   
$EndComp
Connection ~ 1500 9700
$Comp
L power:GND #PWR0102
U 1 1 5E047EC2
P 600 8650
F 0 "#PWR0102" H 600 8400 50  0001 C CNN
F 1 "GND" H 600 8500 50  0000 C CNN
F 2 "" H 600 8650 50  0001 C CNN
F 3 "" H 600 8650 50  0001 C CNN
	1    600  8650
	1    0    0    -1  
$EndComp
Wire Wire Line
	650  8550 600  8550
Wire Wire Line
	600  8550 600  8650
Wire Wire Line
	2400 10450 2700 10450
Wire Wire Line
	2700 10450 2700 9250
Wire Wire Line
	2700 9250 2450 9250
Wire Wire Line
	2400 10550 2800 10550
Wire Wire Line
	2800 10550 2800 9350
Wire Wire Line
	2800 9350 2350 9350
Wire Wire Line
	2350 9350 2350 9250
Wire Wire Line
	2150 9450 2150 9250
Wire Wire Line
	2050 9250 2050 9550
Wire Wire Line
	3100 9550 3100 10950
$Comp
L Device:C C3
U 1 1 5E1346DE
P 2600 10650
F 0 "C3" V 2550 10750 50  0000 C CNN
F 1 "100n" V 2650 10800 50  0000 C CNN
F 2 "Capacitors_SMD:C_0805" H 2638 10500 50  0001 C CNN
F 3 "~" H 2600 10650 50  0001 C CNN
	1    2600 10650
	0    1    1    0   
$EndComp
$Comp
L Device:C C4
U 1 1 5E1347D0
P 2600 10850
F 0 "C4" V 2550 10950 50  0000 C CNN
F 1 "100n" V 2650 11000 50  0000 C CNN
F 2 "Capacitors_SMD:C_0805" H 2638 10700 50  0001 C CNN
F 3 "~" H 2600 10850 50  0001 C CNN
	1    2600 10850
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0103
U 1 1 5E134818
P 2800 10650
F 0 "#PWR0103" H 2800 10400 50  0001 C CNN
F 1 "GND" H 2800 10500 50  0000 C CNN
F 2 "" H 2800 10650 50  0001 C CNN
F 3 "" H 2800 10650 50  0001 C CNN
	1    2800 10650
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0104
U 1 1 5E134871
P 2800 10850
F 0 "#PWR0104" H 2800 10600 50  0001 C CNN
F 1 "GND" H 2800 10700 50  0000 C CNN
F 2 "" H 2800 10850 50  0001 C CNN
F 3 "" H 2800 10850 50  0001 C CNN
	1    2800 10850
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3000 10750 3000 9450
Wire Wire Line
	2400 10750 3000 10750
Wire Wire Line
	2150 9450 3000 9450
Wire Wire Line
	3100 9550 2050 9550
Wire Wire Line
	2400 10950 3100 10950
Wire Wire Line
	2450 10650 2400 10650
Wire Wire Line
	2450 10850 2400 10850
$Comp
L Device:R R2
U 1 1 5E28B358
P 2400 9800
F 0 "R2" V 2400 9750 50  0000 L CNN
F 1 "6K49" V 2300 9750 50  0000 L CNN
F 2 "Resistors_SMD:R_0805" V 2330 9800 50  0001 C CNN
F 3 "~" H 2400 9800 50  0001 C CNN
	1    2400 9800
	1    0    0    -1  
$EndComp
$Comp
L Device:R R3
U 1 1 5E28B45C
P 2550 9800
F 0 "R3" V 2550 9750 50  0000 L CNN
F 1 "6K49" V 2650 9700 50  0000 L CNN
F 2 "Resistors_SMD:R_0805" V 2480 9800 50  0001 C CNN
F 3 "~" H 2550 9800 50  0001 C CNN
	1    2550 9800
	1    0    0    -1  
$EndComp
Wire Wire Line
	2400 10150 2550 10150
Wire Wire Line
	2550 10150 2550 9950
Wire Wire Line
	2400 10050 2450 10050
Wire Wire Line
	2450 10050 2450 10250
Wire Wire Line
	2450 10250 2400 10250
$Comp
L power:+3.3V #PWR0105
U 1 1 5E2EEF91
P 2450 10250
F 0 "#PWR0105" H 2450 10100 50  0001 C CNN
F 1 "+3.3V" V 2500 10250 50  0000 L CNN
F 2 "" H 2450 10250 50  0001 C CNN
F 3 "" H 2450 10250 50  0001 C CNN
	1    2450 10250
	0    1    1    0   
$EndComp
Connection ~ 2450 10250
Wire Wire Line
	850  7850 850  7750
Wire Wire Line
	850  7750 750  7750
Wire Wire Line
	950  7850 950  7750
Wire Wire Line
	950  7750 1050 7750
$Comp
L Device:C C1
U 1 1 5E355A02
P 750 7150
F 0 "C1" H 900 7050 50  0000 R CNN
F 1 "20p" H 900 7250 50  0000 R CNN
F 2 "Capacitors_SMD:C_0805" H 788 7000 50  0001 C CNN
F 3 "~" H 750 7150 50  0001 C CNN
	1    750  7150
	-1   0    0    1   
$EndComp
$Comp
L Device:C C2
U 1 1 5E355BF6
P 1050 7150
F 0 "C2" H 1000 7050 50  0000 R CNN
F 1 "20p" H 1000 7250 50  0000 R CNN
F 2 "Capacitors_SMD:C_0805" H 1088 7000 50  0001 C CNN
F 3 "~" H 1050 7150 50  0001 C CNN
	1    1050 7150
	-1   0    0    1   
$EndComp
Wire Wire Line
	1050 7000 900  7000
Connection ~ 900  7000
Wire Wire Line
	900  7000 750  7000
$Comp
L power:+3.3V #PWR0106
U 1 1 5E428DCF
P 2850 8200
F 0 "#PWR0106" H 2850 8050 50  0001 C CNN
F 1 "+3.3V" H 2750 8350 50  0000 L CNN
F 2 "" H 2850 8200 50  0001 C CNN
F 3 "" H 2850 8200 50  0001 C CNN
	1    2850 8200
	1    0    0    -1  
$EndComp
Wire Wire Line
	2750 8550 2850 8550
Wire Wire Line
	2850 8550 2850 8200
Wire Wire Line
	2750 8650 2850 8650
Wire Wire Line
	2850 8650 2850 8550
Connection ~ 2850 8550
$Comp
L Device:C C5
U 1 1 5E4CAF5C
P 2850 8800
F 0 "C5" H 2750 8900 50  0000 L CNN
F 1 "2,2µ" V 2800 8600 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 2888 8650 50  0001 C CNN
F 3 "~" H 2850 8800 50  0001 C CNN
	1    2850 8800
	1    0    0    -1  
$EndComp
Connection ~ 2850 8650
$Comp
L Device:C C6
U 1 1 5E4CB000
P 3050 8800
F 0 "C6" H 2950 8900 50  0000 L CNN
F 1 "2,2µ" V 3000 8600 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 3088 8650 50  0001 C CNN
F 3 "~" H 3050 8800 50  0001 C CNN
	1    3050 8800
	1    0    0    -1  
$EndComp
$Comp
L Device:C C7
U 1 1 5E4CB05A
P 3250 8800
F 0 "C7" H 3150 8900 50  0000 L CNN
F 1 "2,2µ" V 3200 8600 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 3288 8650 50  0001 C CNN
F 3 "~" H 3250 8800 50  0001 C CNN
	1    3250 8800
	1    0    0    -1  
$EndComp
$Comp
L Device:C C8
U 1 1 5E4CB0B2
P 3450 8800
F 0 "C8" H 3350 8900 50  0000 L CNN
F 1 "2,2µ" V 3400 8600 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 3488 8650 50  0001 C CNN
F 3 "~" H 3450 8800 50  0001 C CNN
	1    3450 8800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0107
U 1 1 5E4CB10A
P 900 7000
F 0 "#PWR0107" H 900 6750 50  0001 C CNN
F 1 "GND" H 900 6850 50  0000 C CNN
F 2 "" H 900 7000 50  0001 C CNN
F 3 "" H 900 7000 50  0001 C CNN
	1    900  7000
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR0108
U 1 1 5E4CB15B
P 2850 8950
F 0 "#PWR0108" H 2850 8700 50  0001 C CNN
F 1 "GND" H 2850 8800 50  0000 C CNN
F 2 "" H 2850 8950 50  0001 C CNN
F 3 "" H 2850 8950 50  0001 C CNN
	1    2850 8950
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0109
U 1 1 5E4CB25B
P 3050 8950
F 0 "#PWR0109" H 3050 8700 50  0001 C CNN
F 1 "GND" H 3050 8800 50  0000 C CNN
F 2 "" H 3050 8950 50  0001 C CNN
F 3 "" H 3050 8950 50  0001 C CNN
	1    3050 8950
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0110
U 1 1 5E4CB2AC
P 3250 8950
F 0 "#PWR0110" H 3250 8700 50  0001 C CNN
F 1 "GND" H 3250 8800 50  0000 C CNN
F 2 "" H 3250 8950 50  0001 C CNN
F 3 "" H 3250 8950 50  0001 C CNN
	1    3250 8950
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0111
U 1 1 5E4CB2FD
P 3450 8950
F 0 "#PWR0111" H 3450 8700 50  0001 C CNN
F 1 "GND" H 3450 8800 50  0000 C CNN
F 2 "" H 3450 8950 50  0001 C CNN
F 3 "" H 3450 8950 50  0001 C CNN
	1    3450 8950
	1    0    0    -1  
$EndComp
Wire Wire Line
	3050 8650 3050 8550
Wire Wire Line
	3050 8550 2850 8550
Wire Wire Line
	3250 8650 3250 8450
Wire Wire Line
	2750 8450 3250 8450
Connection ~ 3250 8450
Text Label 2550 7600 3    50   ~ 0
TX_EN
Text Label 2450 7600 3    50   ~ 0
TX_D0
Text Label 2350 7600 3    50   ~ 0
TX_D1
Text Label 2150 7600 3    50   ~ 0
RX_D0
Text Label 2050 7600 3    50   ~ 0
RX_D1
Text Label 1850 7600 3    50   ~ 0
CLK
Text Label 1450 7650 3    50   ~ 0
MDC
Text Label 1550 7650 3    50   ~ 0
MDIO
Wire Wire Line
	2550 7600 2550 7850
Wire Wire Line
	2450 7600 2450 7850
Wire Wire Line
	2350 7600 2350 7850
Wire Wire Line
	2150 7600 2150 7850
Wire Wire Line
	2050 7600 2050 7850
Wire Wire Line
	1850 7600 1850 7850
Wire Wire Line
	1550 7650 1550 7850
Wire Wire Line
	1450 7650 1450 7850
$Comp
L Device:R R1
U 1 1 5E7347D9
P 1100 9300
F 0 "R1" V 1100 9300 50  0000 C CNN
F 1 "6K49" V 1200 9300 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 1030 9300 50  0001 C CNN
F 3 "~" H 1100 9300 50  0001 C CNN
	1    1100 9300
	0    1    1    0   
$EndComp
Wire Wire Line
	1250 9250 1250 9300
$Comp
L power:GND #PWR0112
U 1 1 5E76E224
P 950 9300
F 0 "#PWR0112" H 950 9050 50  0001 C CNN
F 1 "GND" H 950 9150 50  0000 C CNN
F 2 "" H 950 9300 50  0001 C CNN
F 3 "" H 950 9300 50  0001 C CNN
	1    950  9300
	0    1    1    0   
$EndComp
Wire Wire Line
	2550 9650 2550 9600
Wire Wire Line
	2550 9600 1950 9600
Wire Wire Line
	1950 9600 1950 9250
Wire Wire Line
	1950 9250 1450 9250
Text Label 1950 7600 3    50   ~ 0
RX_DV
Wire Wire Line
	1950 7600 1950 7850
Text Label 1750 7600 3    50   ~ 0
RX_ER
Wire Wire Line
	1750 7600 1750 7850
Text Label 1150 7700 3    50   ~ 0
RST
Wire Wire Line
	1150 7700 1150 7850
Text Label 1250 7550 3    50   ~ 0
ETH_INT
Wire Wire Line
	1250 7850 1250 7550
Text Label 7950 4300 3    50   ~ 0
MDC
Text Label 4850 4250 3    50   ~ 0
ETH_INT
Text Label 4450 7150 1    50   ~ 0
RST
Text Label 4650 4350 3    50   ~ 0
MDIO
Text Label 5150 4300 3    50   ~ 0
RX_DV
Text Label 4550 4400 3    50   ~ 0
CLK
Text Label 8350 4300 3    50   ~ 0
RX_D1
Text Label 8250 4300 3    50   ~ 0
RX_D0
Text Label 7450 4300 3    50   ~ 0
TX_D1
Text Label 7350 4300 3    50   ~ 0
TX_D0
Text Label 7250 4300 3    50   ~ 0
TX_EN
Text Label 2350 7200 3    50   ~ 0
RX_ER
Wire Wire Line
	4550 4400 4550 4550
Wire Wire Line
	4650 4350 4650 4550
Wire Wire Line
	4850 4250 4850 4550
Wire Wire Line
	5150 4300 5150 4550
Wire Wire Line
	7250 4300 7250 4550
Wire Wire Line
	7350 4300 7350 4550
Wire Wire Line
	7450 4300 7450 4550
Wire Wire Line
	7950 4300 7950 4550
Wire Wire Line
	8250 4300 8250 4550
Wire Wire Line
	8350 4300 8350 4550
Wire Wire Line
	4450 7150 4450 6950
Wire Wire Line
	3450 8650 3450 8450
Wire Wire Line
	3250 8450 3450 8450
Wire Wire Line
	4650 8150 4750 8150
Connection ~ 6950 8150
Connection ~ 4750 8150
Wire Wire Line
	4750 8150 4850 8150
Connection ~ 4850 8150
Wire Wire Line
	4850 8150 4950 8150
Connection ~ 4950 8150
Wire Wire Line
	4950 8150 5050 8150
Connection ~ 5050 8150
Wire Wire Line
	5050 8150 5150 8150
Connection ~ 5150 8150
Wire Wire Line
	5150 8150 5250 8150
Connection ~ 5250 8150
Wire Wire Line
	5250 8150 5350 8150
Connection ~ 5350 8150
Wire Wire Line
	5350 8150 5450 8150
Connection ~ 5450 8150
Wire Wire Line
	5450 8150 5550 8150
Connection ~ 5550 8150
Wire Wire Line
	5550 8150 5650 8150
Connection ~ 5650 8150
Wire Wire Line
	5650 8150 5750 8150
Connection ~ 5750 8150
Wire Wire Line
	5750 8150 5850 8150
Connection ~ 5850 8150
Wire Wire Line
	5850 8150 5950 8150
Connection ~ 5950 8150
Wire Wire Line
	5950 8150 6050 8150
Connection ~ 6050 8150
Wire Wire Line
	6050 8150 6150 8150
Connection ~ 6150 8150
Wire Wire Line
	6150 8150 6250 8150
Connection ~ 6250 8150
Wire Wire Line
	6250 8150 6350 8150
Connection ~ 6350 8150
Wire Wire Line
	6350 8150 6450 8150
Connection ~ 6450 8150
Wire Wire Line
	6450 8150 6550 8150
Connection ~ 6550 8150
Wire Wire Line
	6550 8150 6650 8150
Connection ~ 6650 8150
Wire Wire Line
	6650 8150 6750 8150
Connection ~ 6750 8150
Wire Wire Line
	6750 8150 6850 8150
Connection ~ 6850 8150
Wire Wire Line
	6850 8150 6950 8150
Wire Wire Line
	7550 8150 7650 8150
Connection ~ 7550 8150
Connection ~ 7650 8150
Wire Wire Line
	7650 8150 7750 8150
Connection ~ 7750 8150
Wire Wire Line
	7750 8150 7850 8150
Connection ~ 7850 8150
Wire Wire Line
	7850 8150 7950 8150
Connection ~ 7950 8150
Wire Wire Line
	7950 8150 8050 8150
Connection ~ 8050 8150
Wire Wire Line
	8050 8150 8150 8150
Connection ~ 8150 8150
Wire Wire Line
	8150 8150 8250 8150
Connection ~ 8250 8150
Wire Wire Line
	8250 8150 8350 8150
Connection ~ 8350 8150
Wire Wire Line
	8350 8150 8450 8150
Connection ~ 8450 8150
Wire Wire Line
	8450 8150 8550 8150
Connection ~ 8550 8150
Wire Wire Line
	8550 8150 8650 8150
Connection ~ 8650 8150
Wire Wire Line
	8650 8150 8750 8150
Connection ~ 8750 8150
Wire Wire Line
	8750 8150 8850 8150
Connection ~ 8850 8150
Wire Wire Line
	8850 8150 8950 8150
Connection ~ 8950 8150
Wire Wire Line
	8950 8150 9050 8150
Connection ~ 9050 8150
Wire Wire Line
	9050 8150 9150 8150
Connection ~ 9150 8150
Wire Wire Line
	9150 8150 9250 8150
Connection ~ 9250 8150
Wire Wire Line
	9250 8150 9350 8150
Connection ~ 9350 8150
Wire Wire Line
	9350 8150 9450 8150
Connection ~ 9450 8150
Wire Wire Line
	9450 8150 9550 8150
Connection ~ 9550 8150
Wire Wire Line
	9550 8150 9650 8150
Connection ~ 9650 8150
Wire Wire Line
	9650 8150 9750 8150
Connection ~ 9750 8150
Wire Wire Line
	9750 8150 9850 8150
Wire Wire Line
	4750 2750 4750 2850
Wire Wire Line
	4750 2850 4850 2850
Wire Wire Line
	4850 2850 4850 2750
Wire Wire Line
	4850 2850 6550 2850
Wire Wire Line
	6550 2850 6550 2750
Connection ~ 4850 2850
Wire Wire Line
	6650 2850 6650 2750
Connection ~ 6650 2850
Wire Wire Line
	8350 2850 8350 2750
Wire Wire Line
	8450 2850 8450 2750
Wire Wire Line
	10150 2850 10150 2750
Wire Wire Line
	10250 2850 10250 2750
Wire Wire Line
	11950 2850 11950 2750
Wire Wire Line
	12050 2850 12050 2750
Text Label 5750 4300 3    50   ~ 0
SWDIO
Wire Wire Line
	5750 4300 5750 4550
Text Label 5850 4300 3    50   ~ 0
SWCLK
Wire Wire Line
	5850 4550 5850 4300
Text Label 8850 4200 3    50   ~ 0
SPI_SCK
Text Label 8950 4200 3    50   ~ 0
SPI_MISO
Text Label 9050 4200 3    50   ~ 0
SPI_MOSI
Wire Wire Line
	8850 4200 8850 4550
Wire Wire Line
	8950 4200 8950 4550
Wire Wire Line
	9050 4200 9050 4550
Text Label 5950 4300 3    50   ~ 0
SPI_CS
Wire Wire Line
	5950 4300 5950 4550
Text Label 10050 4200 3    50   ~ 0
UART_TX
Text Label 10150 4200 3    50   ~ 0
UART_RX
Wire Wire Line
	10150 4200 10150 4550
Wire Wire Line
	10050 4200 10050 4550
Text Label 9750 7200 1    50   ~ 0
TX3
Wire Wire Line
	9750 7200 9750 6950
Text Label 1850 7200 3    50   ~ 0
TX3
Text Label 8050 4300 3    50   ~ 0
TX2
Text Label 8150 4300 3    50   ~ 0
TX_CLK
Wire Wire Line
	8050 4300 8050 4550
Wire Wire Line
	8150 4300 8150 4550
Text Label 2450 7200 3    50   ~ 0
TX_CLK
Text Label 1950 7200 3    50   ~ 0
TX2
Text Label 4450 4400 3    50   ~ 0
CRS
Text Label 1700 7200 3    50   ~ 0
CRS
Wire Wire Line
	4450 4400 4450 4550
Wire Wire Line
	4750 4350 4750 4550
Text Label 4750 4350 3    50   ~ 0
COL
Text Label 1600 7200 3    50   ~ 0
COL
Text Label 6150 4350 3    50   ~ 0
RX2
Wire Wire Line
	6150 4350 6150 4550
Text Label 2200 7200 3    50   ~ 0
RX2
Wire Wire Line
	6250 4550 6250 4350
Text Label 6250 4350 3    50   ~ 0
RX3
Text Label 2100 7200 3    50   ~ 0
RX3
Text Label 12650 2850 2    50   ~ 0
OUTP_EN
Text Label 7150 4200 3    50   ~ 0
OUTP_EN
Wire Wire Line
	7150 4200 7150 4550
$Comp
L Device:R R4
U 1 1 5F5337D2
P 12250 2700
F 0 "R4" H 12150 2600 50  0000 C CNN
F 1 "6K49" H 12100 2700 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 12180 2700 50  0001 C CNN
F 3 "~" H 12250 2700 50  0001 C CNN
	1    12250 2700
	-1   0    0    1   
$EndComp
Text Label 12700 8850 0    50   ~ 0
+5V
$Comp
L power:+5V #PWR0113
U 1 1 5F581A81
P 12950 8850
F 0 "#PWR0113" H 12950 8700 50  0001 C CNN
F 1 "+5V" H 12950 9000 50  0000 C CNN
F 2 "" H 12950 8850 50  0001 C CNN
F 3 "" H 12950 8850 50  0001 C CNN
	1    12950 8850
	1    0    0    -1  
$EndComp
Wire Wire Line
	12700 8850 12950 8850
$Comp
L power:+5V #PWR0114
U 1 1 5F5CF786
P 3550 2250
F 0 "#PWR0114" H 3550 2100 50  0001 C CNN
F 1 "+5V" H 3550 2400 50  0000 C CNN
F 2 "" H 3550 2250 50  0001 C CNN
F 3 "" H 3550 2250 50  0001 C CNN
	1    3550 2250
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0115
U 1 1 5F5CF86F
P 5350 2250
F 0 "#PWR0115" H 5350 2100 50  0001 C CNN
F 1 "+5V" H 5350 2400 50  0000 C CNN
F 2 "" H 5350 2250 50  0001 C CNN
F 3 "" H 5350 2250 50  0001 C CNN
	1    5350 2250
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0116
U 1 1 5F5CF8C4
P 12250 2550
F 0 "#PWR0116" H 12250 2400 50  0001 C CNN
F 1 "+5V" H 12250 2700 50  0000 C CNN
F 2 "" H 12250 2550 50  0001 C CNN
F 3 "" H 12250 2550 50  0001 C CNN
	1    12250 2550
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0117
U 1 1 5F5CFB29
P 10750 2250
F 0 "#PWR0117" H 10750 2100 50  0001 C CNN
F 1 "+5V" H 10750 2400 50  0000 C CNN
F 2 "" H 10750 2250 50  0001 C CNN
F 3 "" H 10750 2250 50  0001 C CNN
	1    10750 2250
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0118
U 1 1 5F5CFB7E
P 8950 2250
F 0 "#PWR0118" H 8950 2100 50  0001 C CNN
F 1 "+5V" H 8950 2400 50  0000 C CNN
F 2 "" H 8950 2250 50  0001 C CNN
F 3 "" H 8950 2250 50  0001 C CNN
	1    8950 2250
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0119
U 1 1 5F5CFC8C
P 7150 2250
F 0 "#PWR0119" H 7150 2100 50  0001 C CNN
F 1 "+5V" H 7150 2400 50  0000 C CNN
F 2 "" H 7150 2250 50  0001 C CNN
F 3 "" H 7150 2250 50  0001 C CNN
	1    7150 2250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0120
U 1 1 5F5D0064
P 12350 2250
F 0 "#PWR0120" H 12350 2000 50  0001 C CNN
F 1 "GND" H 12450 2150 50  0000 C CNN
F 2 "" H 12350 2250 50  0001 C CNN
F 3 "" H 12350 2250 50  0001 C CNN
	1    12350 2250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0121
U 1 1 5F5D0241
P 8750 2250
F 0 "#PWR0121" H 8750 2000 50  0001 C CNN
F 1 "GND" H 8650 2150 50  0000 C CNN
F 2 "" H 8750 2250 50  0001 C CNN
F 3 "" H 8750 2250 50  0001 C CNN
	1    8750 2250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0122
U 1 1 5F5D0481
P 6950 2250
F 0 "#PWR0122" H 6950 2000 50  0001 C CNN
F 1 "GND" H 6850 2150 50  0000 C CNN
F 2 "" H 6950 2250 50  0001 C CNN
F 3 "" H 6950 2250 50  0001 C CNN
	1    6950 2250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0123
U 1 1 5F5D05B5
P 5150 2250
F 0 "#PWR0123" H 5150 2000 50  0001 C CNN
F 1 "GND" H 5050 2150 50  0000 C CNN
F 2 "" H 5150 2250 50  0001 C CNN
F 3 "" H 5150 2250 50  0001 C CNN
	1    5150 2250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0124
U 1 1 5F5D069E
P 10550 2250
F 0 "#PWR0124" H 10550 2000 50  0001 C CNN
F 1 "GND" H 10450 2150 50  0000 C CNN
F 2 "" H 10550 2250 50  0001 C CNN
F 3 "" H 10550 2250 50  0001 C CNN
	1    10550 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	4650 8550 4650 9200
$Comp
L Device:R R6
U 1 1 5CB29C37
P 14900 9050
F 0 "R6" V 14900 9050 50  0000 C CNN
F 1 "6K49" V 14800 9050 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 14830 9050 50  0001 C CNN
F 3 "~" H 14900 9050 50  0001 C CNN
	1    14900 9050
	0    1    1    0   
$EndComp
$Comp
L Device:C C37
U 1 1 5CB2A2AE
P 15050 9200
F 0 "C37" H 15050 9300 50  0000 L CNN
F 1 "100n" H 15050 9100 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 15088 9050 50  0001 C CNN
F 3 "~" H 15050 9200 50  0001 C CNN
	1    15050 9200
	1    0    0    -1  
$EndComp
Text Label 15250 9050 2    50   ~ 0
RST
Wire Wire Line
	15250 9050 15050 9050
Connection ~ 15050 9050
$Comp
L power:GND #PWR0125
U 1 1 5CB78B47
P 15050 9350
F 0 "#PWR0125" H 15050 9100 50  0001 C CNN
F 1 "GND" H 15050 9200 50  0000 C CNN
F 2 "" H 15050 9350 50  0001 C CNN
F 3 "" H 15050 9350 50  0001 C CNN
	1    15050 9350
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0126
U 1 1 5CB78DC2
P 14650 9050
F 0 "#PWR0126" H 14650 8900 50  0001 C CNN
F 1 "+3.3V" H 14665 9223 50  0000 C CNN
F 2 "" H 14650 9050 50  0001 C CNN
F 3 "" H 14650 9050 50  0001 C CNN
	1    14650 9050
	1    0    0    -1  
$EndComp
Wire Wire Line
	14750 9050 14650 9050
$Comp
L Regulator_Linear:LM1117-3.3 U8
U 1 1 5CBC738B
P 13500 9050
F 0 "U8" H 13500 9292 50  0000 C CNN
F 1 "LM1117DTX-3.3" H 13500 9201 50  0000 C CNN
F 2 "TO_SOT_Packages_SMD:TO-252-3_TabPin2" H 13500 9050 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm1117.pdf" H 13500 9050 50  0001 C CNN
	1    13500 9050
	1    0    0    -1  
$EndComp
$Comp
L Device:C C35
U 1 1 5CC157DE
P 13900 9200
F 0 "C35" H 13750 9300 50  0000 L CNN
F 1 "100n" H 13700 9100 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 13938 9050 50  0001 C CNN
F 3 "~" H 13900 9200 50  0001 C CNN
	1    13900 9200
	1    0    0    -1  
$EndComp
$Comp
L Device:C C36
U 1 1 5CC1587C
P 14150 9200
F 0 "C36" H 14150 9300 50  0000 L CNN
F 1 "2,2µ" H 14150 9100 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 14188 9050 50  0001 C CNN
F 3 "~" H 14150 9200 50  0001 C CNN
	1    14150 9200
	1    0    0    -1  
$EndComp
$Comp
L Device:C C34
U 1 1 5CC158F2
P 13100 9200
F 0 "C34" H 13100 9300 50  0000 L CNN
F 1 "100n" H 13100 9100 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 13138 9050 50  0001 C CNN
F 3 "~" H 13100 9200 50  0001 C CNN
	1    13100 9200
	1    0    0    -1  
$EndComp
$Comp
L Device:C C33
U 1 1 5CC15996
P 12850 9200
F 0 "C33" H 12650 9300 50  0000 L CNN
F 1 "2,2µ" H 12650 9100 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 12888 9050 50  0001 C CNN
F 3 "~" H 12850 9200 50  0001 C CNN
	1    12850 9200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0127
U 1 1 5CC15A04
P 14150 9350
F 0 "#PWR0127" H 14150 9100 50  0001 C CNN
F 1 "GND" H 14150 9200 50  0000 C CNN
F 2 "" H 14150 9350 50  0001 C CNN
F 3 "" H 14150 9350 50  0001 C CNN
	1    14150 9350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0128
U 1 1 5CC15A67
P 13900 9350
F 0 "#PWR0128" H 13900 9100 50  0001 C CNN
F 1 "GND" H 13900 9200 50  0000 C CNN
F 2 "" H 13900 9350 50  0001 C CNN
F 3 "" H 13900 9350 50  0001 C CNN
	1    13900 9350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0129
U 1 1 5CC15ACA
P 13500 9350
F 0 "#PWR0129" H 13500 9100 50  0001 C CNN
F 1 "GND" H 13500 9200 50  0000 C CNN
F 2 "" H 13500 9350 50  0001 C CNN
F 3 "" H 13500 9350 50  0001 C CNN
	1    13500 9350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0130
U 1 1 5CC15B2D
P 13100 9350
F 0 "#PWR0130" H 13100 9100 50  0001 C CNN
F 1 "GND" H 13100 9200 50  0000 C CNN
F 2 "" H 13100 9350 50  0001 C CNN
F 3 "" H 13100 9350 50  0001 C CNN
	1    13100 9350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0131
U 1 1 5CC15B90
P 12850 9350
F 0 "#PWR0131" H 12850 9100 50  0001 C CNN
F 1 "GND" H 12850 9200 50  0000 C CNN
F 2 "" H 12850 9350 50  0001 C CNN
F 3 "" H 12850 9350 50  0001 C CNN
	1    12850 9350
	1    0    0    -1  
$EndComp
Wire Wire Line
	13200 9050 13100 9050
Wire Wire Line
	13100 9050 12850 9050
Connection ~ 13100 9050
Connection ~ 12850 9050
Wire Wire Line
	13800 9050 13900 9050
Wire Wire Line
	13900 9050 14150 9050
Connection ~ 13900 9050
Wire Wire Line
	14150 9050 14650 9050
Connection ~ 14150 9050
Connection ~ 14650 9050
$Comp
L Device:C C25
U 1 1 5CDA009A
P 5350 2400
F 0 "C25" H 5350 2500 50  0000 L CNN
F 1 "2,2µ" H 5350 2300 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 5388 2250 50  0001 C CNN
F 3 "~" H 5350 2400 50  0001 C CNN
	1    5350 2400
	1    0    0    -1  
$EndComp
Connection ~ 5350 2250
$Comp
L Device:C C9
U 1 1 5CDA0584
P 3550 2400
F 0 "C9" H 3550 2500 50  0000 L CNN
F 1 "2,2µ" H 3550 2300 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 3588 2250 50  0001 C CNN
F 3 "~" H 3550 2400 50  0001 C CNN
	1    3550 2400
	1    0    0    -1  
$EndComp
Connection ~ 3550 2250
$Comp
L Device:C C30
U 1 1 5CDA0626
P 7150 2400
F 0 "C30" H 7150 2500 50  0000 L CNN
F 1 "2,2µ" H 7150 2300 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 7188 2250 50  0001 C CNN
F 3 "~" H 7150 2400 50  0001 C CNN
	1    7150 2400
	1    0    0    -1  
$EndComp
Connection ~ 7150 2250
$Comp
L Device:C C31
U 1 1 5CDA084D
P 8950 2400
F 0 "C31" H 8950 2500 50  0000 L CNN
F 1 "2,2µ" H 8950 2300 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 8988 2250 50  0001 C CNN
F 3 "~" H 8950 2400 50  0001 C CNN
	1    8950 2400
	1    0    0    -1  
$EndComp
Connection ~ 8950 2250
$Comp
L Device:C C32
U 1 1 5CDA0913
P 10750 2400
F 0 "C32" H 10750 2500 50  0000 L CNN
F 1 "2,2µ" H 10750 2300 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 10788 2250 50  0001 C CNN
F 3 "~" H 10750 2400 50  0001 C CNN
	1    10750 2400
	1    0    0    -1  
$EndComp
Connection ~ 10750 2250
$Comp
L power:GND #PWR0133
U 1 1 5CDA11D8
P 3550 2550
F 0 "#PWR0133" H 3550 2300 50  0001 C CNN
F 1 "GND" H 3450 2450 50  0000 C CNN
F 2 "" H 3550 2550 50  0001 C CNN
F 3 "" H 3550 2550 50  0001 C CNN
	1    3550 2550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0134
U 1 1 5CDA1245
P 5350 2550
F 0 "#PWR0134" H 5350 2300 50  0001 C CNN
F 1 "GND" H 5450 2450 50  0000 C CNN
F 2 "" H 5350 2550 50  0001 C CNN
F 3 "" H 5350 2550 50  0001 C CNN
	1    5350 2550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0135
U 1 1 5CDA12B2
P 7150 2550
F 0 "#PWR0135" H 7150 2300 50  0001 C CNN
F 1 "GND" H 7250 2450 50  0000 C CNN
F 2 "" H 7150 2550 50  0001 C CNN
F 3 "" H 7150 2550 50  0001 C CNN
	1    7150 2550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0136
U 1 1 5CDA1445
P 8950 2550
F 0 "#PWR0136" H 8950 2300 50  0001 C CNN
F 1 "GND" H 9050 2450 50  0000 C CNN
F 2 "" H 8950 2550 50  0001 C CNN
F 3 "" H 8950 2550 50  0001 C CNN
	1    8950 2550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0137
U 1 1 5CDA14B2
P 10750 2550
F 0 "#PWR0137" H 10750 2300 50  0001 C CNN
F 1 "GND" H 10850 2450 50  0000 C CNN
F 2 "" H 10750 2550 50  0001 C CNN
F 3 "" H 10750 2550 50  0001 C CNN
	1    10750 2550
	1    0    0    -1  
$EndComp
$Comp
L Device:C C24
U 1 1 5CDA1891
P 4850 7100
F 0 "C24" H 4850 7200 50  0000 L CNN
F 1 "2,2µ" V 4800 6900 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 4888 6950 50  0001 C CNN
F 3 "~" H 4850 7100 50  0001 C CNN
	1    4850 7100
	1    0    0    -1  
$EndComp
$Comp
L Device:C C26
U 1 1 5CDA1BBF
P 5050 7100
F 0 "C26" H 5050 7200 50  0000 L CNN
F 1 "2,2µ" V 5000 6900 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 5088 6950 50  0001 C CNN
F 3 "~" H 5050 7100 50  0001 C CNN
	1    5050 7100
	1    0    0    -1  
$EndComp
Wire Wire Line
	4950 6950 4850 6950
$Comp
L power:GND #PWR0138
U 1 1 5CDF1484
P 4850 7250
F 0 "#PWR0138" H 4850 7000 50  0001 C CNN
F 1 "GND" H 4850 7100 50  0000 C CNN
F 2 "" H 4850 7250 50  0001 C CNN
F 3 "" H 4850 7250 50  0001 C CNN
	1    4850 7250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0139
U 1 1 5CDF15F4
P 5050 7250
F 0 "#PWR0139" H 5050 7000 50  0001 C CNN
F 1 "GND" H 5100 7100 50  0000 C CNN
F 2 "" H 5050 7250 50  0001 C CNN
F 3 "" H 5050 7250 50  0001 C CNN
	1    5050 7250
	1    0    0    -1  
$EndComp
$Comp
L Device:C C27
U 1 1 5CDF16CB
P 5250 7100
F 0 "C27" H 5250 7200 50  0000 L CNN
F 1 "2,2µ" V 5200 6900 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 5288 6950 50  0001 C CNN
F 3 "~" H 5250 7100 50  0001 C CNN
	1    5250 7100
	1    0    0    -1  
$EndComp
Wire Wire Line
	5150 6950 5250 6950
$Comp
L power:GND #PWR0140
U 1 1 5CE41330
P 5250 7250
F 0 "#PWR0140" H 5250 7000 50  0001 C CNN
F 1 "GND" H 5350 7150 50  0000 C CNN
F 2 "" H 5250 7250 50  0001 C CNN
F 3 "" H 5250 7250 50  0001 C CNN
	1    5250 7250
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0141
U 1 1 5CE414A7
P 5250 6950
F 0 "#PWR0141" H 5250 6800 50  0001 C CNN
F 1 "+3.3V" H 5300 7050 50  0000 L CNN
F 2 "" H 5250 6950 50  0001 C CNN
F 3 "" H 5250 6950 50  0001 C CNN
	1    5250 6950
	1    0    0    -1  
$EndComp
Connection ~ 5250 6950
$Comp
L Device:C C23
U 1 1 5CE41CB2
P 3350 6850
F 0 "C23" V 3400 6950 50  0000 L CNN
F 1 "100n" V 3400 6600 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 3388 6700 50  0001 C CNN
F 3 "~" H 3350 6850 50  0001 C CNN
	1    3350 6850
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C22
U 1 1 5CE4285E
P 3350 6650
F 0 "C22" V 3400 6750 50  0000 L CNN
F 1 "100n" V 3400 6400 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 3388 6500 50  0001 C CNN
F 3 "~" H 3350 6650 50  0001 C CNN
	1    3350 6650
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C21
U 1 1 5CE428E6
P 3350 6450
F 0 "C21" V 3400 6550 50  0000 L CNN
F 1 "100n" V 3400 6200 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 3388 6300 50  0001 C CNN
F 3 "~" H 3350 6450 50  0001 C CNN
	1    3350 6450
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C20
U 1 1 5CE42966
P 3350 6250
F 0 "C20" V 3400 6350 50  0000 L CNN
F 1 "100n" V 3400 6000 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 3388 6100 50  0001 C CNN
F 3 "~" H 3350 6250 50  0001 C CNN
	1    3350 6250
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C19
U 1 1 5CE429EA
P 3350 6050
F 0 "C19" V 3400 6150 50  0000 L CNN
F 1 "100n" V 3400 5800 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 3388 5900 50  0001 C CNN
F 3 "~" H 3350 6050 50  0001 C CNN
	1    3350 6050
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C18
U 1 1 5CE42A76
P 3350 5850
F 0 "C18" V 3400 5950 50  0000 L CNN
F 1 "100n" V 3400 5600 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 3388 5700 50  0001 C CNN
F 3 "~" H 3350 5850 50  0001 C CNN
	1    3350 5850
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C17
U 1 1 5CE42B00
P 3350 5650
F 0 "C17" V 3400 5750 50  0000 L CNN
F 1 "100n" V 3400 5400 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 3388 5500 50  0001 C CNN
F 3 "~" H 3350 5650 50  0001 C CNN
	1    3350 5650
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C16
U 1 1 5CE42B88
P 3350 5450
F 0 "C16" V 3400 5550 50  0000 L CNN
F 1 "100n" V 3400 5200 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 3388 5300 50  0001 C CNN
F 3 "~" H 3350 5450 50  0001 C CNN
	1    3350 5450
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C15
U 1 1 5CE42C14
P 3350 5250
F 0 "C15" V 3400 5350 50  0000 L CNN
F 1 "100n" V 3400 5000 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 3388 5100 50  0001 C CNN
F 3 "~" H 3350 5250 50  0001 C CNN
	1    3350 5250
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C14
U 1 1 5CE42CA0
P 3350 5050
F 0 "C14" V 3400 5150 50  0000 L CNN
F 1 "100n" V 3400 4800 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 3388 4900 50  0001 C CNN
F 3 "~" H 3350 5050 50  0001 C CNN
	1    3350 5050
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C13
U 1 1 5CE42D30
P 3350 4850
F 0 "C13" V 3400 4950 50  0000 L CNN
F 1 "100n" V 3400 4600 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 3388 4700 50  0001 C CNN
F 3 "~" H 3350 4850 50  0001 C CNN
	1    3350 4850
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C12
U 1 1 5CE42DC4
P 3350 4650
F 0 "C12" V 3400 4750 50  0000 L CNN
F 1 "100n" V 3400 4400 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 3388 4500 50  0001 C CNN
F 3 "~" H 3350 4650 50  0001 C CNN
	1    3350 4650
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C11
U 1 1 5CE42E58
P 3350 4450
F 0 "C11" V 3400 4550 50  0000 L CNN
F 1 "100n" V 3400 4200 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 3388 4300 50  0001 C CNN
F 3 "~" H 3350 4450 50  0001 C CNN
	1    3350 4450
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C10
U 1 1 5CE42EEE
P 3350 4250
F 0 "C10" V 3400 4350 50  0000 L CNN
F 1 "100n" V 3400 4000 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 3388 4100 50  0001 C CNN
F 3 "~" H 3350 4250 50  0001 C CNN
	1    3350 4250
	0    -1   -1   0   
$EndComp
$Comp
L power:+3.3V #PWR0142
U 1 1 5CE42FCB
P 3650 4150
F 0 "#PWR0142" H 3650 4000 50  0001 C CNN
F 1 "+3.3V" H 3650 4300 50  0000 C CNN
F 2 "" H 3650 4150 50  0001 C CNN
F 3 "" H 3650 4150 50  0001 C CNN
	1    3650 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	3500 4250 3650 4250
Wire Wire Line
	3500 4450 3650 4450
Wire Wire Line
	3500 4650 3650 4650
Wire Wire Line
	3500 4850 3650 4850
Wire Wire Line
	3500 5050 3650 5050
Wire Wire Line
	3500 5250 3650 5250
Wire Wire Line
	3500 5450 3650 5450
Wire Wire Line
	3500 5650 3650 5650
Wire Wire Line
	3500 5850 3650 5850
Wire Wire Line
	3500 6050 3650 6050
Wire Wire Line
	3500 6250 3650 6250
Wire Wire Line
	3500 6450 3650 6450
Wire Wire Line
	3500 6650 3650 6650
Wire Wire Line
	3500 6850 3650 6850
Wire Wire Line
	3050 4250 3200 4250
Wire Wire Line
	3050 4450 3200 4450
Wire Wire Line
	3050 4650 3200 4650
Wire Wire Line
	3050 4850 3200 4850
Wire Wire Line
	3050 5050 3200 5050
Wire Wire Line
	3050 5250 3200 5250
Wire Wire Line
	3050 5450 3200 5450
Wire Wire Line
	3050 5650 3200 5650
Wire Wire Line
	3050 5850 3200 5850
Wire Wire Line
	3050 6050 3200 6050
Wire Wire Line
	3050 6250 3200 6250
Wire Wire Line
	3050 6450 3200 6450
Wire Wire Line
	3050 6650 3200 6650
Wire Wire Line
	3050 6850 3200 6850
Wire Wire Line
	3650 4150 3650 4250
Connection ~ 3650 4250
Wire Wire Line
	3650 4250 3650 4450
Connection ~ 3650 4450
Wire Wire Line
	3650 4450 3650 4650
Connection ~ 3650 4650
Wire Wire Line
	3650 4650 3650 4850
Connection ~ 3650 4850
Wire Wire Line
	3650 4850 3650 5050
Connection ~ 3650 5050
Wire Wire Line
	3650 5050 3650 5250
Connection ~ 3650 5250
Wire Wire Line
	3650 5250 3650 5450
Connection ~ 3650 5450
Wire Wire Line
	3650 5450 3650 5650
Connection ~ 3650 5650
Wire Wire Line
	3650 5650 3650 5850
Connection ~ 3650 5850
Wire Wire Line
	3650 5850 3650 6050
Connection ~ 3650 6050
Wire Wire Line
	3650 6050 3650 6250
Connection ~ 3650 6250
Wire Wire Line
	3650 6250 3650 6450
Connection ~ 3650 6450
Wire Wire Line
	3650 6450 3650 6650
Connection ~ 3650 6650
Wire Wire Line
	3650 6650 3650 6850
$Comp
L power:GND #PWR0143
U 1 1 5CF93781
P 3050 6950
F 0 "#PWR0143" H 3050 6700 50  0001 C CNN
F 1 "GND" H 3050 6800 50  0000 C CNN
F 2 "" H 3050 6950 50  0001 C CNN
F 3 "" H 3050 6950 50  0001 C CNN
	1    3050 6950
	1    0    0    -1  
$EndComp
Wire Wire Line
	3050 6950 3050 6850
Connection ~ 3050 4450
Wire Wire Line
	3050 4450 3050 4250
Connection ~ 3050 4650
Wire Wire Line
	3050 4650 3050 4450
Connection ~ 3050 4850
Wire Wire Line
	3050 4850 3050 4650
Connection ~ 3050 5050
Wire Wire Line
	3050 5050 3050 4850
Connection ~ 3050 5250
Wire Wire Line
	3050 5250 3050 5050
Connection ~ 3050 5450
Wire Wire Line
	3050 5450 3050 5250
Connection ~ 3050 5650
Wire Wire Line
	3050 5650 3050 5450
Connection ~ 3050 5850
Wire Wire Line
	3050 5850 3050 5650
Connection ~ 3050 6050
Wire Wire Line
	3050 6050 3050 5850
Connection ~ 3050 6250
Wire Wire Line
	3050 6250 3050 6050
Connection ~ 3050 6450
Wire Wire Line
	3050 6450 3050 6250
Connection ~ 3050 6650
Wire Wire Line
	3050 6650 3050 6450
Connection ~ 3050 6850
Wire Wire Line
	3050 6850 3050 6650
Wire Wire Line
	3950 6350 3950 6250
Connection ~ 3950 6250
$Comp
L power:+3.3V #PWR0144
U 1 1 5D0B17C4
P 4550 7000
F 0 "#PWR0144" H 4550 6850 50  0001 C CNN
F 1 "+3.3V" V 4600 7000 50  0000 L CNN
F 2 "" H 4550 7000 50  0001 C CNN
F 3 "" H 4550 7000 50  0001 C CNN
	1    4550 7000
	-1   0    0    1   
$EndComp
$Comp
L Device:R R5
U 1 1 5D0B1AE7
P 4700 7100
F 0 "R5" V 4700 7100 50  0000 C CNN
F 1 "6K49 n_best" V 4600 7100 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 4630 7100 50  0001 C CNN
F 3 "~" H 4700 7100 50  0001 C CNN
	1    4700 7100
	1    0    0    -1  
$EndComp
Wire Wire Line
	4750 6950 4700 6950
$Comp
L power:GND #PWR0145
U 1 1 5D113026
P 4700 7250
F 0 "#PWR0145" H 4700 7000 50  0001 C CNN
F 1 "GND" H 4650 7100 50  0000 C CNN
F 2 "" H 4700 7250 50  0001 C CNN
F 3 "" H 4700 7250 50  0001 C CNN
	1    4700 7250
	1    0    0    -1  
$EndComp
$Comp
L Device:C C29
U 1 1 5D113BD0
P 6050 7700
F 0 "C29" H 6050 7600 50  0000 R CNN
F 1 "20p" H 6050 7800 50  0000 R CNN
F 2 "Capacitors_SMD:C_0805" H 6088 7550 50  0001 C CNN
F 3 "~" H 6050 7700 50  0001 C CNN
	1    6050 7700
	-1   0    0    1   
$EndComp
$Comp
L Device:C C28
U 1 1 5D113E55
P 5750 7700
F 0 "C28" H 5900 7500 50  0000 R CNN
F 1 "20p" H 5900 7600 50  0000 R CNN
F 2 "Capacitors_SMD:C_0805" H 5788 7550 50  0001 C CNN
F 3 "~" H 5750 7700 50  0001 C CNN
	1    5750 7700
	-1   0    0    1   
$EndComp
Wire Wire Line
	5950 6950 6050 6950
Wire Wire Line
	5850 6950 5750 6950
Wire Wire Line
	6050 7850 5900 7850
Connection ~ 5900 7850
$Comp
L power:GND #PWR0146
U 1 1 5D3C8D12
P 5900 7850
F 0 "#PWR0146" H 5900 7600 50  0001 C CNN
F 1 "GND" H 6000 7750 50  0000 C CNN
F 2 "" H 5900 7850 50  0001 C CNN
F 3 "" H 5900 7850 50  0001 C CNN
	1    5900 7850
	1    0    0    -1  
$EndComp
Wire Wire Line
	5750 7850 5900 7850
Connection ~ 1050 7000
Wire Wire Line
	1100 7000 1050 7000
Wire Wire Line
	1100 7700 1100 7000
Wire Wire Line
	900  7700 1100 7700
Wire Wire Line
	900  7300 900  7000
Connection ~ 5750 7850
Wire Wire Line
	6050 6950 6050 7200
Connection ~ 6050 7200
Wire Wire Line
	5750 6950 5750 7200
Connection ~ 5750 7200
$Comp
L Device:Crystal_GND24 Y2
U 1 1 5D1138F6
P 5900 7200
F 0 "Y2" H 5950 7400 50  0000 L CNN
F 1 "25MHz" V 5900 7150 25  0000 L CNN
F 2 "Crystals:Crystal_SMD_SeikoEpson_TSX3225-4pin_3.2x2.5mm" H 5900 7200 50  0001 C CNN
F 3 "~" H 5900 7200 50  0001 C CNN
	1    5900 7200
	1    0    0    -1  
$EndComp
$Comp
L Device:Crystal_GND24 Y1
U 1 1 5D5C2B5F
P 900 7500
F 0 "Y1" H 950 7700 50  0000 L CNN
F 1 "25MHz" V 900 7450 25  0000 L CNN
F 2 "Crystals:Crystal_SMD_SeikoEpson_TSX3225-4pin_3.2x2.5mm" H 900 7500 50  0001 C CNN
F 3 "~" H 900 7500 50  0001 C CNN
	1    900  7500
	1    0    0    -1  
$EndComp
Wire Wire Line
	750  7300 750  7500
Wire Wire Line
	1050 7300 1050 7500
Connection ~ 750  7500
Wire Wire Line
	750  7500 750  7750
Connection ~ 1050 7500
Wire Wire Line
	1050 7500 1050 7750
Text Label 4950 3950 3    50   ~ 0
PA5
Text Label 5050 3950 3    50   ~ 0
PA6
Text Label 5250 3950 3    50   ~ 0
PA8
Text Label 5350 3950 3    50   ~ 0
PA9
Text Label 5450 3950 3    50   ~ 0
PA10
Text Label 5550 3950 3    50   ~ 0
PA11
Text Label 5650 3950 3    50   ~ 0
PA12
Wire Wire Line
	4950 3950 4950 4550
Wire Wire Line
	5050 4550 5050 3950
Wire Wire Line
	5250 3950 5250 4550
Wire Wire Line
	5350 3950 5350 4550
Wire Wire Line
	5450 4550 5450 3950
Wire Wire Line
	5550 4550 5550 3950
Wire Wire Line
	5650 4550 5650 3950
Wire Wire Line
	6350 3950 6350 4550
Wire Wire Line
	6450 3950 6450 4550
Wire Wire Line
	6550 4550 6550 3950
Wire Wire Line
	6650 4550 6650 3950
Wire Wire Line
	6750 4550 6750 3950
Wire Wire Line
	10650 3950 10650 4550
Wire Wire Line
	10750 3950 10750 4550
Wire Wire Line
	10850 4550 10850 3950
Wire Wire Line
	10950 4550 10950 3950
Wire Wire Line
	11050 4550 11050 3950
Wire Wire Line
	10250 3950 10250 4550
Wire Wire Line
	10350 4550 10350 3950
Wire Wire Line
	10450 4550 10450 3950
Wire Wire Line
	10550 4550 10550 3950
Wire Wire Line
	9150 4550 9150 3950
Wire Wire Line
	9250 4550 9250 3950
Wire Wire Line
	9350 4550 9350 3950
Wire Wire Line
	9850 4550 9850 3950
Wire Wire Line
	9950 4550 9950 3950
Wire Wire Line
	8650 4550 8650 3950
Wire Wire Line
	8750 4550 8750 3950
Wire Wire Line
	8450 4550 8450 3950
Wire Wire Line
	8550 4550 8550 3950
Wire Wire Line
	7550 4550 7550 3950
Wire Wire Line
	7850 4550 7850 3950
Wire Wire Line
	6950 4550 6950 3950
Wire Wire Line
	7050 4550 7050 3950
Wire Wire Line
	7650 4550 7650 3950
Wire Wire Line
	6850 4550 6850 3950
Wire Wire Line
	6150 6950 6150 7550
Wire Wire Line
	6250 6950 6250 7550
Wire Wire Line
	6350 7550 6350 6950
Wire Wire Line
	6450 7550 6450 6950
Wire Wire Line
	6550 7550 6550 6950
Wire Wire Line
	6750 7550 6750 6950
Wire Wire Line
	6850 7550 6850 6950
Wire Wire Line
	6650 7550 6650 6950
Wire Wire Line
	6950 6950 6950 7550
Wire Wire Line
	7050 6950 7050 7550
Wire Wire Line
	7150 7550 7150 6950
Wire Wire Line
	7250 7550 7250 6950
Wire Wire Line
	7350 7550 7350 6950
Wire Wire Line
	7550 7550 7550 6950
Wire Wire Line
	7650 7550 7650 6950
Wire Wire Line
	7450 7550 7450 6950
Wire Wire Line
	7850 6950 7850 7550
Wire Wire Line
	7950 6950 7950 7550
Wire Wire Line
	8050 7550 8050 6950
Wire Wire Line
	8150 7550 8150 6950
Wire Wire Line
	8250 7550 8250 6950
Wire Wire Line
	8450 7550 8450 6950
Wire Wire Line
	8550 7550 8550 6950
Wire Wire Line
	8350 7550 8350 6950
Wire Wire Line
	8650 6950 8650 7550
Wire Wire Line
	8750 6950 8750 7550
Wire Wire Line
	8850 7550 8850 6950
Wire Wire Line
	8950 7550 8950 6950
Wire Wire Line
	9050 7550 9050 6950
Wire Wire Line
	9250 7550 9250 6950
Wire Wire Line
	9350 7550 9350 6950
Wire Wire Line
	9150 7550 9150 6950
Wire Wire Line
	10350 6950 10350 7550
Wire Wire Line
	10450 6950 10450 7550
Wire Wire Line
	10550 7550 10550 6950
Wire Wire Line
	10650 7550 10650 6950
Wire Wire Line
	10750 7550 10750 6950
Wire Wire Line
	10950 7550 10950 6950
Wire Wire Line
	11050 7550 11050 6950
Wire Wire Line
	10850 7550 10850 6950
Wire Wire Line
	9850 6950 9850 7550
Wire Wire Line
	9950 6950 9950 7550
Wire Wire Line
	10050 7550 10050 6950
Wire Wire Line
	10150 7550 10150 6950
Wire Wire Line
	10250 7550 10250 6950
Wire Wire Line
	9550 7550 9550 6950
Wire Wire Line
	9650 7550 9650 6950
Text Label 6350 3950 3    50   ~ 0
PB2
Text Label 6450 3950 3    50   ~ 0
PB3
Text Label 6550 3950 3    50   ~ 0
PB4
Text Label 6650 3950 3    50   ~ 0
PB5
Text Label 6750 3950 3    50   ~ 0
PB6
Text Label 6850 3950 3    50   ~ 0
PB7
Text Label 6950 3950 3    50   ~ 0
PB8
Text Label 7050 3950 3    50   ~ 0
PB9
Text Label 7550 3950 3    50   ~ 0
PB14
Text Label 7650 3950 3    50   ~ 0
PB15
Text Label 7850 3950 3    50   ~ 0
PC0
Text Label 8450 3950 3    50   ~ 0
PC6
Text Label 8550 3950 3    50   ~ 0
PC7
Text Label 8650 3950 3    50   ~ 0
PC8
Text Label 8750 3950 3    50   ~ 0
PC9
Text Label 9150 3950 3    50   ~ 0
PC13
Text Label 9250 3950 3    50   ~ 0
PC14
Text Label 9350 3950 3    50   ~ 0
PC15
Text Label 9850 3950 3    50   ~ 0
PD3
Text Label 9950 3950 3    50   ~ 0
PD4
Text Label 10250 3950 3    50   ~ 0
PD7
Text Label 10350 3950 3    50   ~ 0
PD8
Text Label 10450 3950 3    50   ~ 0
PD9
Text Label 10550 3950 3    50   ~ 0
PD10
Text Label 10650 3950 3    50   ~ 0
PD11
Text Label 10750 3950 3    50   ~ 0
PD12
Text Label 10850 3950 3    50   ~ 0
PD13
Text Label 10950 3950 3    50   ~ 0
PD14
Text Label 11050 3950 3    50   ~ 0
PD15
Text Label 9550 7550 1    50   ~ 0
PE0
Text Label 9650 7550 1    50   ~ 0
PE1
Text Label 9850 7550 1    50   ~ 0
PE3
Text Label 9950 7550 1    50   ~ 0
PE4
Text Label 10050 7550 1    50   ~ 0
PE5
Text Label 10150 7550 1    50   ~ 0
PE6
Text Label 10250 7550 1    50   ~ 0
PE7
Text Label 10350 7550 1    50   ~ 0
PE8
Text Label 10450 7550 1    50   ~ 0
PE9
Text Label 10550 7550 1    50   ~ 0
PE10
Text Label 10650 7550 1    50   ~ 0
PE11
Text Label 10750 7550 1    50   ~ 0
PE12
Text Label 10850 7550 1    50   ~ 0
PE13
Text Label 10950 7550 1    50   ~ 0
PE14
Text Label 11050 7550 1    50   ~ 0
PE15
Text Label 7850 7550 1    50   ~ 0
PF0
Text Label 7950 7550 1    50   ~ 0
PF1
Text Label 8050 7550 1    50   ~ 0
PF2
Text Label 8150 7550 1    50   ~ 0
PF3
Text Label 8250 7550 1    50   ~ 0
PF4
Text Label 8350 7550 1    50   ~ 0
PF5
Text Label 8450 7550 1    50   ~ 0
PF6
Text Label 8550 7550 1    50   ~ 0
PF7
Text Label 8650 7550 1    50   ~ 0
PF8
Text Label 8750 7550 1    50   ~ 0
PF9
Text Label 8850 7550 1    50   ~ 0
PF10
Text Label 8950 7550 1    50   ~ 0
PF11
Text Label 9050 7550 1    50   ~ 0
PF12
Text Label 9150 7550 1    50   ~ 0
PF13
Text Label 9250 7550 1    50   ~ 0
PF14
Text Label 9350 7550 1    50   ~ 0
PF15
Text Label 6150 7550 1    50   ~ 0
PG0
Text Label 6250 7550 1    50   ~ 0
PG1
Text Label 6350 7550 1    50   ~ 0
PG2
Text Label 6450 7550 1    50   ~ 0
PG3
Text Label 6550 7550 1    50   ~ 0
PG4
Text Label 6650 7550 1    50   ~ 0
PG5
Text Label 6750 7550 1    50   ~ 0
PG6
Text Label 6850 7550 1    50   ~ 0
PG7
Text Label 6950 7550 1    50   ~ 0
PG8
Text Label 7050 7550 1    50   ~ 0
PG9
Text Label 7150 7550 1    50   ~ 0
PG10
Text Label 7250 7550 1    50   ~ 0
PG11
Text Label 7350 7550 1    50   ~ 0
PG12
Text Label 7450 7550 1    50   ~ 0
PG13
Text Label 7550 7550 1    50   ~ 0
PG14
Text Label 7650 7550 1    50   ~ 0
PG15
Wire Wire Line
	5900 7400 5900 7850
Wire Wire Line
	6050 7200 6050 7550
Wire Wire Line
	5750 7200 5750 7550
$Comp
L Driver_Motor:Pololu_Breakout_A4988 A1
U 1 1 5E7370F5
P 13650 1950
F 0 "A1" H 13500 1750 50  0000 C CNN
F 1 "Pololu_Breakout_A4988" V 13750 1850 50  0000 C CNN
F 2 "Modules:Pololu_Breakout-16_15.2x20.3mm" H 13925 1200 50  0001 L CNN
F 3 "https://www.pololu.com/product/2980/pictures" H 13750 1650 50  0001 C CNN
	1    13650 1950
	1    0    0    -1  
$EndComp
$Comp
L Driver_Motor:Pololu_Breakout_A4988 A2
U 1 1 5E737F6B
P 15200 1950
F 0 "A2" H 15100 1750 50  0000 C CNN
F 1 "Pololu_Breakout_A4988" V 15300 1850 50  0000 C CNN
F 2 "Modules:Pololu_Breakout-16_15.2x20.3mm" H 15475 1200 50  0001 L CNN
F 3 "https://www.pololu.com/product/2980/pictures" H 15300 1650 50  0001 C CNN
	1    15200 1950
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x04_Male J4
U 1 1 5E738280
P 14350 2050
F 0 "J4" H 14400 1800 50  0000 R CNN
F 1 "Conn_01x04_Male" V 14300 2300 50  0001 R CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x04_Pitch2.54mm" H 14350 2050 50  0001 C CNN
F 3 "~" H 14350 2050 50  0001 C CNN
	1    14350 2050
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x04_Male J5
U 1 1 5E738418
P 15900 2050
F 0 "J5" H 15900 1800 50  0000 R CNN
F 1 "Conn_01x04_Male" V 15850 2300 50  0001 R CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x04_Pitch2.54mm" H 15900 2050 50  0001 C CNN
F 3 "~" H 15900 2050 50  0001 C CNN
	1    15900 2050
	-1   0    0    1   
$EndComp
$Comp
L Device:CP C40
U 1 1 5E7389DB
P 14000 1150
F 0 "C40" V 14150 1000 50  0000 C CNN
F 1 "33µ" V 14050 1000 50  0000 C CNN
F 2 "Capacitors_SMD:CP_Elec_8x10" H 14038 1000 50  0001 C CNN
F 3 "~" H 14000 1150 50  0001 C CNN
	1    14000 1150
	0    -1   -1   0   
$EndComp
$Comp
L power:+5V #PWR0132
U 1 1 5E739628
P 12500 9050
F 0 "#PWR0132" H 12500 8900 50  0001 C CNN
F 1 "+5V" H 12515 9223 50  0000 C CNN
F 2 "" H 12500 9050 50  0001 C CNN
F 3 "" H 12500 9050 50  0001 C CNN
	1    12500 9050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0147
U 1 1 5E7396D1
P 12500 9350
F 0 "#PWR0147" H 12500 9100 50  0001 C CNN
F 1 "GND" H 12500 9200 50  0000 C CNN
F 2 "" H 12500 9350 50  0001 C CNN
F 3 "" H 12500 9350 50  0001 C CNN
	1    12500 9350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0148
U 1 1 5E7398B6
P 14650 9350
F 0 "#PWR0148" H 14650 9100 50  0001 C CNN
F 1 "GND" H 14650 9200 50  0000 C CNN
F 2 "" H 14650 9350 50  0001 C CNN
F 3 "" H 14650 9350 50  0001 C CNN
	1    14650 9350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0149
U 1 1 5E739BD7
P 14150 1150
F 0 "#PWR0149" H 14150 900 50  0001 C CNN
F 1 "GND" H 14150 1000 50  0000 C CNN
F 2 "" H 14150 1150 50  0001 C CNN
F 3 "" H 14150 1150 50  0001 C CNN
	1    14150 1150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0150
U 1 1 5E739F47
P 15700 1150
F 0 "#PWR0150" H 15700 900 50  0001 C CNN
F 1 "GND" H 15800 1050 50  0000 C CNN
F 2 "" H 15700 1150 50  0001 C CNN
F 3 "" H 15700 1150 50  0001 C CNN
	1    15700 1150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0151
U 1 1 5E73A307
P 14350 1150
F 0 "#PWR0151" H 14350 900 50  0001 C CNN
F 1 "GND" H 14450 1050 50  0000 C CNN
F 2 "" H 14350 1150 50  0001 C CNN
F 3 "" H 14350 1150 50  0001 C CNN
	1    14350 1150
	1    0    0    -1  
$EndComp
Wire Wire Line
	13850 1250 13850 1150
Connection ~ 13850 1150
$Comp
L power:+3.3V #PWR0152
U 1 1 5E8B421B
P 13650 1150
F 0 "#PWR0152" H 13650 1000 50  0001 C CNN
F 1 "+3.3V" H 13650 1300 50  0000 C CNN
F 2 "" H 13650 1150 50  0001 C CNN
F 3 "" H 13650 1150 50  0001 C CNN
	1    13650 1150
	1    0    0    -1  
$EndComp
$Comp
L Device:C C39
U 1 1 5E8B48A1
P 13500 1200
F 0 "C39" V 13350 950 50  0000 L CNN
F 1 "2,2µ" V 13450 950 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 13538 1050 50  0001 C CNN
F 3 "~" H 13500 1200 50  0001 C CNN
	1    13500 1200
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0153
U 1 1 5E8B507E
P 13350 1200
F 0 "#PWR0153" H 13350 950 50  0001 C CNN
F 1 "GND" H 13250 1100 50  0000 C CNN
F 2 "" H 13350 1200 50  0001 C CNN
F 3 "" H 13350 1200 50  0001 C CNN
	1    13350 1200
	1    0    0    -1  
$EndComp
Wire Wire Line
	13650 1250 13650 1200
Connection ~ 13650 1200
Wire Wire Line
	13650 1200 13650 1150
$Comp
L power:+3.3V #PWR0154
U 1 1 5E9349C2
P 15200 1150
F 0 "#PWR0154" H 15200 1000 50  0001 C CNN
F 1 "+3.3V" H 15250 1300 50  0000 C CNN
F 2 "" H 15200 1150 50  0001 C CNN
F 3 "" H 15200 1150 50  0001 C CNN
	1    15200 1150
	1    0    0    -1  
$EndComp
$Comp
L Device:C C42
U 1 1 5E9349C8
P 15050 1200
F 0 "C42" V 14900 950 50  0000 L CNN
F 1 "2,2µ" V 15000 950 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 15088 1050 50  0001 C CNN
F 3 "~" H 15050 1200 50  0001 C CNN
	1    15050 1200
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0155
U 1 1 5E9349CF
P 14900 1200
F 0 "#PWR0155" H 14900 950 50  0001 C CNN
F 1 "GND" H 14800 1100 50  0000 C CNN
F 2 "" H 14900 1200 50  0001 C CNN
F 3 "" H 14900 1200 50  0001 C CNN
	1    14900 1200
	1    0    0    -1  
$EndComp
Wire Wire Line
	15200 1250 15200 1200
Connection ~ 15200 1200
Wire Wire Line
	15200 1200 15200 1150
$Comp
L power:GND #PWR0156
U 1 1 5EA34F45
P 13650 2750
F 0 "#PWR0156" H 13650 2500 50  0001 C CNN
F 1 "GND" H 13650 2600 50  0000 C CNN
F 2 "" H 13650 2750 50  0001 C CNN
F 3 "" H 13650 2750 50  0001 C CNN
	1    13650 2750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0157
U 1 1 5EA35094
P 13850 2750
F 0 "#PWR0157" H 13850 2500 50  0001 C CNN
F 1 "GND" H 13850 2600 50  0000 C CNN
F 2 "" H 13850 2750 50  0001 C CNN
F 3 "" H 13850 2750 50  0001 C CNN
	1    13850 2750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0158
U 1 1 5EA35141
P 15200 2750
F 0 "#PWR0158" H 15200 2500 50  0001 C CNN
F 1 "GND" H 15200 2600 50  0000 C CNN
F 2 "" H 15200 2750 50  0001 C CNN
F 3 "" H 15200 2750 50  0001 C CNN
	1    15200 2750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0159
U 1 1 5EA35290
P 15400 2750
F 0 "#PWR0159" H 15400 2500 50  0001 C CNN
F 1 "GND" H 15400 2600 50  0000 C CNN
F 2 "" H 15400 2750 50  0001 C CNN
F 3 "" H 15400 2750 50  0001 C CNN
	1    15400 2750
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C43
U 1 1 5EA37183
P 15550 1150
F 0 "C43" V 15700 1000 50  0000 C CNN
F 1 "33µ" V 15600 1000 50  0000 C CNN
F 2 "Capacitors_SMD:CP_Elec_8x10" H 15588 1000 50  0001 C CNN
F 3 "~" H 15550 1150 50  0001 C CNN
	1    15550 1150
	0    -1   -1   0   
$EndComp
Connection ~ 15400 1150
Wire Wire Line
	15400 1150 15400 1250
$Comp
L Device:CP C41
U 1 1 5EA37265
P 14650 9200
F 0 "C41" H 14650 9300 50  0000 L CNN
F 1 "33µ" H 14650 9100 50  0000 L CNN
F 2 "Capacitors_SMD:CP_Elec_8x10" H 14688 9050 50  0001 C CNN
F 3 "~" H 14650 9200 50  0001 C CNN
	1    14650 9200
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C38
U 1 1 5EB37720
P 12500 9200
F 0 "C38" H 12300 9350 50  0000 L CNN
F 1 "33µ" H 12300 9100 50  0000 L CNN
F 2 "Capacitors_SMD:CP_Elec_8x10" H 12538 9050 50  0001 C CNN
F 3 "~" H 12500 9200 50  0001 C CNN
	1    12500 9200
	1    0    0    -1  
$EndComp
Wire Wire Line
	12850 9050 12500 9050
Connection ~ 12500 9050
$Comp
L Connector:Micro_SD_Card J7
U 1 1 5EBB97DE
P 14900 7300
F 0 "J7" H 14850 8017 100 0000 C CNN
F 1 "Micro_SD_Card" H 14850 7926 50  0000 C CNN
F 2 "Connectors:microSD_Card_Receptacle_Wuerth_693072010801" H 16050 7600 50  0001 C CNN
F 3 "http://katalog.we-online.de/em/datasheet/693072010801.pdf" H 14900 7300 50  0001 C CNN
	1    14900 7300
	1    0    0    -1  
$EndComp
Text Label 13650 7400 0    50   ~ 0
SPI_SCK
Text Label 13650 7600 0    50   ~ 0
SPI_MISO
Text Label 13650 7200 0    50   ~ 0
SPI_MOSI
Text Label 13650 7100 0    50   ~ 0
SPI_CS
Wire Wire Line
	13650 7100 14000 7100
Wire Wire Line
	14000 7200 13650 7200
Wire Wire Line
	14000 7400 13650 7400
Wire Wire Line
	13650 7600 14000 7600
$Comp
L power:+3.3V #PWR0160
U 1 1 5EFC18CD
P 13550 6950
F 0 "#PWR0160" H 13550 6800 50  0001 C CNN
F 1 "+3.3V" H 13565 7123 50  0000 C CNN
F 2 "" H 13550 6950 50  0001 C CNN
F 3 "" H 13550 6950 50  0001 C CNN
	1    13550 6950
	1    0    0    -1  
$EndComp
Wire Wire Line
	14000 7300 13550 7300
Wire Wire Line
	13550 7300 13550 6950
$Comp
L power:GND #PWR0161
U 1 1 5F043249
P 13550 7800
F 0 "#PWR0161" H 13550 7550 50  0001 C CNN
F 1 "GND" H 13650 7700 50  0000 C CNN
F 2 "" H 13550 7800 50  0001 C CNN
F 3 "" H 13550 7800 50  0001 C CNN
	1    13550 7800
	1    0    0    -1  
$EndComp
Wire Wire Line
	14000 7500 13550 7500
Wire Wire Line
	13550 7500 13550 7800
$Comp
L power:GND #PWR0162
U 1 1 5F0C53EE
P 15700 7900
F 0 "#PWR0162" H 15700 7650 50  0001 C CNN
F 1 "GND" H 15700 7750 50  0000 C CNN
F 2 "" H 15700 7900 50  0001 C CNN
F 3 "" H 15700 7900 50  0001 C CNN
	1    15700 7900
	1    0    0    -1  
$EndComp
$Comp
L Interface_USB:FT230XQ U9
U 1 1 5F0C6571
P 2300 1600
F 0 "U9" H 2300 1750 100 0000 C CNN
F 1 "FT230XQ" H 2300 1650 50  0000 C CNN
F 2 "Housings_DFN_QFN:QFN-16-1EP_4x4mm_Pitch0.65mm" H 2850 900 50  0001 C CNN
F 3 "http://www.ftdichip.com/Products/ICs/FT230X.html" H 2300 1600 50  0001 C CNN
	1    2300 1600
	1    0    0    -1  
$EndComp
$Comp
L Connector:USB_B_Micro J6
U 1 1 5F0C68C9
P 800 1450
F 0 "J6" H 855 1917 50  0000 C CNN
F 1 "USB_B_Micro" H 855 1826 50  0000 C CNN
F 2 "Connectors:USB_Micro-B_10103594-0001LF" H 950 1400 50  0001 C CNN
F 3 "~" H 950 1400 50  0001 C CNN
	1    800  1450
	1    0    0    -1  
$EndComp
Wire Wire Line
	1600 1500 1300 1500
Wire Wire Line
	1300 1500 1300 1550
Wire Wire Line
	1300 1550 1100 1550
Wire Wire Line
	1600 1600 1350 1600
Wire Wire Line
	1350 1600 1350 1450
Wire Wire Line
	1350 1450 1100 1450
$Comp
L power:GND #PWR0163
U 1 1 5F1CFB12
P 2300 2400
F 0 "#PWR0163" H 2300 2150 50  0001 C CNN
F 1 "GND" H 2200 2300 50  0000 C CNN
F 2 "" H 2300 2400 50  0001 C CNN
F 3 "" H 2300 2400 50  0001 C CNN
	1    2300 2400
	1    0    0    -1  
$EndComp
Wire Wire Line
	2300 2300 2300 2350
Wire Wire Line
	2200 2300 2200 2350
Wire Wire Line
	2200 2350 2300 2350
Wire Wire Line
	2400 2350 2400 2300
Connection ~ 2300 2350
Wire Wire Line
	2300 2350 2300 2400
Wire Wire Line
	2300 2350 2400 2350
$Comp
L power:GND #PWR0164
U 1 1 5F2DAC30
P 750 1950
F 0 "#PWR0164" H 750 1700 50  0001 C CNN
F 1 "GND" H 850 1850 50  0000 C CNN
F 2 "" H 750 1950 50  0001 C CNN
F 3 "" H 750 1950 50  0001 C CNN
	1    750  1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	700  1850 700  1900
Wire Wire Line
	700  1900 750  1900
Wire Wire Line
	800  1900 800  1850
Wire Wire Line
	750  1950 750  1900
Connection ~ 750  1900
Wire Wire Line
	750  1900 800  1900
$Comp
L Device:C C45
U 1 1 5F4FE656
P 1550 1950
F 0 "C45" H 1350 2050 50  0000 L CNN
F 1 "100n" H 1350 1850 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 1588 1800 50  0001 C CNN
F 3 "~" H 1550 1950 50  0001 C CNN
	1    1550 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	1600 1800 1550 1800
Wire Wire Line
	1600 1200 1550 1200
$Comp
L power:GND #PWR0165
U 1 1 5F61308D
P 1550 2100
F 0 "#PWR0165" H 1550 1850 50  0001 C CNN
F 1 "GND" H 1450 2000 50  0000 C CNN
F 2 "" H 1550 2100 50  0001 C CNN
F 3 "" H 1550 2100 50  0001 C CNN
	1    1550 2100
	1    0    0    -1  
$EndComp
Wire Wire Line
	2400 9650 1900 9650
Text Label 2250 9750 2    50   ~ 0
USB_LNK
Wire Wire Line
	2250 9750 1900 9750
Wire Wire Line
	1900 9750 1900 9650
Text Label 3400 1800 2    50   ~ 0
USB_LNK
Wire Wire Line
	3400 1800 3000 1800
$Comp
L Device:R R7
U 1 1 5F9E7501
P 3150 1900
F 0 "R7" V 3150 1850 50  0000 L CNN
F 1 "6K49" V 3250 1800 50  0000 L CNN
F 2 "Resistors_SMD:R_0805" V 3080 1900 50  0001 C CNN
F 3 "~" H 3150 1900 50  0001 C CNN
	1    3150 1900
	0    1    1    0   
$EndComp
Text Label 3350 1300 2    50   ~ 0
UART_TX
Text Label 3350 1200 2    50   ~ 0
UART_RX
Wire Wire Line
	3000 1200 3350 1200
Wire Wire Line
	3000 1300 3350 1300
Wire Wire Line
	2400 650  2400 900 
Wire Wire Line
	2200 900  2200 800 
Wire Wire Line
	2200 800  1300 800 
Wire Wire Line
	1200 800  1200 1250
Wire Wire Line
	1200 1250 1100 1250
$Comp
L Device:C C44
U 1 1 5FC24F1D
P 1300 950
F 0 "C44" H 1300 1050 50  0000 L CNN
F 1 "2,2µ" H 1300 850 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 1338 800 50  0001 C CNN
F 3 "~" H 1300 950 50  0001 C CNN
	1    1300 950 
	1    0    0    -1  
$EndComp
Connection ~ 1300 800 
Wire Wire Line
	1300 800  1200 800 
$Comp
L power:GND #PWR0167
U 1 1 5FC25045
P 1300 1100
F 0 "#PWR0167" H 1300 850 50  0001 C CNN
F 1 "GND" H 1300 950 50  0000 C CNN
F 2 "" H 1300 1100 50  0001 C CNN
F 3 "" H 1300 1100 50  0001 C CNN
	1    1300 1100
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 1750 6350 1300
Wire Wire Line
	2400 650  3550 650 
Wire Wire Line
	3550 650  3550 1900
Wire Wire Line
	3550 1900 3300 1900
Connection ~ 2400 650 
Wire Wire Line
	13250 1650 12950 1650
Wire Wire Line
	13250 1850 12950 1850
Wire Wire Line
	13250 1950 12950 1950
Wire Wire Line
	13250 2050 12950 2050
Text Label 12950 1950 0    50   ~ 0
PB9
Text Label 12950 1850 0    50   ~ 0
PE0
Wire Wire Line
	14800 1650 14500 1650
Wire Wire Line
	14800 1850 14500 1850
Wire Wire Line
	14800 1950 14500 1950
Wire Wire Line
	14800 2050 14500 2050
Text Label 14500 1950 0    50   ~ 0
PB8
Text Label 12950 2050 0    50   ~ 0
PB7
Text Label 14500 1850 0    50   ~ 0
PB4
Text Label 14500 1650 0    50   ~ 0
PB5
Wire Wire Line
	3850 2750 3850 3200
Wire Wire Line
	3950 2750 3950 3200
Wire Wire Line
	4050 2750 4050 3200
Wire Wire Line
	4150 2750 4150 3200
Wire Wire Line
	4250 2750 4250 3200
Wire Wire Line
	4350 2750 4350 3200
Wire Wire Line
	4450 2750 4450 3200
Wire Wire Line
	5650 2750 5650 3200
Text Label 3850 3200 1    50   ~ 0
PE3
Text Label 3950 3200 1    50   ~ 0
PE4
Text Label 4050 3200 1    50   ~ 0
PE5
Text Label 4150 3200 1    50   ~ 0
PE6
Text Label 4250 3200 1    50   ~ 0
PC13
Text Label 4350 3200 1    50   ~ 0
PC14
Text Label 4450 3200 1    50   ~ 0
PC15
Text Label 4550 3200 1    50   ~ 0
PF0
Wire Wire Line
	9750 3950 9750 4550
Wire Wire Line
	9650 3950 9650 4550
Wire Wire Line
	9550 3950 9550 4550
Text Label 9550 3950 3    50   ~ 0
PD0
Text Label 9650 3950 3    50   ~ 0
PD1
Text Label 9750 3950 3    50   ~ 0
PD2
Connection ~ 6550 2850
Wire Wire Line
	6550 2850 6650 2850
Wire Wire Line
	6650 2850 8350 2850
Connection ~ 8350 2850
Wire Wire Line
	8350 2850 8450 2850
Connection ~ 8450 2850
Wire Wire Line
	8450 2850 10150 2850
Connection ~ 10150 2850
Wire Wire Line
	10150 2850 10250 2850
Connection ~ 10250 2850
Wire Wire Line
	10250 2850 11950 2850
Connection ~ 11950 2850
Wire Wire Line
	11950 2850 12050 2850
Connection ~ 12050 2850
Wire Wire Line
	12050 2850 12250 2850
Wire Wire Line
	12650 2850 12250 2850
Connection ~ 12250 2850
Text Label 12850 2250 0    50   ~ 0
STP1_MS1
Text Label 12850 2350 0    50   ~ 0
STP1_MS2
Text Label 12850 2450 0    50   ~ 0
STP1_MS3
Wire Wire Line
	13250 2250 12850 2250
Wire Wire Line
	12850 2350 13250 2350
Wire Wire Line
	12850 2450 13250 2450
Text Label 14400 2250 0    50   ~ 0
STP2_MS1
Text Label 14400 2350 0    50   ~ 0
STP2_MS2
Text Label 14400 2450 0    50   ~ 0
STP2_MS3
Wire Wire Line
	14400 2250 14800 2250
Wire Wire Line
	14400 2350 14800 2350
Wire Wire Line
	14400 2450 14800 2450
Text Label 13000 3350 2    50   ~ 0
STP1_MS1
Text Label 13000 3850 2    50   ~ 0
STP1_MS2
Text Label 13000 4350 2    50   ~ 0
STP1_MS3
Wire Wire Line
	12650 3350 13000 3350
Wire Wire Line
	13000 3850 12650 3850
Wire Wire Line
	12650 4350 13000 4350
Wire Wire Line
	12500 3150 12250 3150
Wire Wire Line
	12250 3150 12250 3650
Wire Wire Line
	12250 3650 12500 3650
Wire Wire Line
	12500 4150 12250 4150
Wire Wire Line
	12250 4150 12250 3650
Connection ~ 12250 3650
Wire Wire Line
	12500 3550 12300 3550
Wire Wire Line
	12300 3550 12300 4050
Wire Wire Line
	12300 4550 12500 4550
Wire Wire Line
	12500 4050 12300 4050
Connection ~ 12300 4050
Wire Wire Line
	12300 4050 12300 4550
$Comp
L power:+3.3V #PWR0166
U 1 1 5F0D49F5
P 12250 3150
F 0 "#PWR0166" H 12250 3000 50  0001 C CNN
F 1 "+3.3V" H 12265 3323 50  0000 C CNN
F 2 "" H 12250 3150 50  0001 C CNN
F 3 "" H 12250 3150 50  0001 C CNN
	1    12250 3150
	1    0    0    -1  
$EndComp
Connection ~ 12250 3150
$Comp
L power:GND #PWR0168
U 1 1 5F0D4B68
P 12300 6050
F 0 "#PWR0168" H 12300 5800 50  0001 C CNN
F 1 "GND" H 12300 5900 50  0000 C CNN
F 2 "" H 12300 6050 50  0001 C CNN
F 3 "" H 12300 6050 50  0001 C CNN
	1    12300 6050
	1    0    0    -1  
$EndComp
Text Label 13000 4850 2    50   ~ 0
STP2_MS1
Text Label 13000 5350 2    50   ~ 0
STP2_MS2
Text Label 13000 5850 2    50   ~ 0
STP2_MS3
Wire Wire Line
	12650 4850 13000 4850
Wire Wire Line
	13000 5350 12650 5350
Wire Wire Line
	12650 5850 13000 5850
Wire Wire Line
	12500 4650 12250 4650
Wire Wire Line
	12250 4650 12250 5150
Wire Wire Line
	12250 5150 12500 5150
Wire Wire Line
	12500 5650 12250 5650
Wire Wire Line
	12250 5650 12250 5150
Connection ~ 12250 5150
Wire Wire Line
	12500 5050 12300 5050
Wire Wire Line
	12300 5050 12300 5550
Wire Wire Line
	12300 6050 12500 6050
Wire Wire Line
	12500 5550 12300 5550
Connection ~ 12300 5550
Wire Wire Line
	12300 5550 12300 6050
Connection ~ 12300 6050
Wire Wire Line
	12300 5050 12300 4550
Connection ~ 12300 5050
Connection ~ 12300 4550
Wire Wire Line
	12250 4650 12250 4150
Connection ~ 12250 4650
Connection ~ 12250 4150
$Comp
L Jumper:SolderJumper_3_Open JP1
U 1 1 5F2BAEA2
P 12500 3350
F 0 "JP1" V 12500 3418 50  0000 L CNN
F 1 "SolderJumper_3_Open" V 12455 3418 50  0001 L CNN
F 2 "Jumper:SolderJumper-3_P1.3mm_Open_Pad1.0x1.5mm" H 12500 3350 50  0001 C CNN
F 3 "~" H 12500 3350 50  0001 C CNN
	1    12500 3350
	0    -1   -1   0   
$EndComp
$Comp
L Jumper:SolderJumper_3_Open JP2
U 1 1 5CB46E69
P 12500 3850
F 0 "JP2" V 12500 3918 50  0000 L CNN
F 1 "SolderJumper_3_Open" V 12455 3918 50  0001 L CNN
F 2 "Jumper:SolderJumper-3_P1.3mm_Open_Pad1.0x1.5mm" H 12500 3850 50  0001 C CNN
F 3 "~" H 12500 3850 50  0001 C CNN
	1    12500 3850
	0    -1   -1   0   
$EndComp
$Comp
L Jumper:SolderJumper_3_Open JP3
U 1 1 5CB46F3F
P 12500 4350
F 0 "JP3" V 12500 4418 50  0000 L CNN
F 1 "SolderJumper_3_Open" V 12455 4418 50  0001 L CNN
F 2 "Jumper:SolderJumper-3_P1.3mm_Open_Pad1.0x1.5mm" H 12500 4350 50  0001 C CNN
F 3 "~" H 12500 4350 50  0001 C CNN
	1    12500 4350
	0    -1   -1   0   
$EndComp
$Comp
L Jumper:SolderJumper_3_Open JP4
U 1 1 5CB47131
P 12500 4850
F 0 "JP4" V 12500 4918 50  0000 L CNN
F 1 "SolderJumper_3_Open" V 12455 4918 50  0001 L CNN
F 2 "Jumper:SolderJumper-3_P1.3mm_Open_Pad1.0x1.5mm" H 12500 4850 50  0001 C CNN
F 3 "~" H 12500 4850 50  0001 C CNN
	1    12500 4850
	0    -1   -1   0   
$EndComp
$Comp
L Jumper:SolderJumper_3_Open JP5
U 1 1 5CB473D6
P 12500 5350
F 0 "JP5" V 12500 5418 50  0000 L CNN
F 1 "SolderJumper_3_Open" V 12455 5418 50  0001 L CNN
F 2 "Jumper:SolderJumper-3_P1.3mm_Open_Pad1.0x1.5mm" H 12500 5350 50  0001 C CNN
F 3 "~" H 12500 5350 50  0001 C CNN
	1    12500 5350
	0    -1   -1   0   
$EndComp
$Comp
L Jumper:SolderJumper_3_Open JP6
U 1 1 5CB474AE
P 12500 5850
F 0 "JP6" V 12500 5918 50  0000 L CNN
F 1 "SolderJumper_3_Open" V 12455 5918 50  0001 L CNN
F 2 "Jumper:SolderJumper-3_P1.3mm_Open_Pad1.0x1.5mm" H 12500 5850 50  0001 C CNN
F 3 "~" H 12500 5850 50  0001 C CNN
	1    12500 5850
	0    -1   -1   0   
$EndComp
Text Label 12950 1550 0    50   ~ 0
RST
Text Label 14500 1550 0    50   ~ 0
RST
Wire Wire Line
	14500 1550 14800 1550
Wire Wire Line
	12950 1550 13250 1550
Text Label 5750 3200 1    50   ~ 0
PF2
Text Label 5850 3200 1    50   ~ 0
PF3
Text Label 5950 3200 1    50   ~ 0
PF4
Text Label 6050 3200 1    50   ~ 0
PF5
Text Label 6150 3200 1    50   ~ 0
PF6
Text Label 6250 3200 1    50   ~ 0
PF7
Text Label 6350 3200 1    50   ~ 0
PF8
Text Label 5650 3200 1    50   ~ 0
PF1
Wire Wire Line
	4550 2750 4550 3200
Wire Wire Line
	5750 3200 5750 2750
Wire Wire Line
	5850 2750 5850 3200
Wire Wire Line
	5950 3200 5950 2750
Wire Wire Line
	6050 2750 6050 3200
Wire Wire Line
	6150 2750 6150 3200
Wire Wire Line
	6250 2750 6250 3200
Wire Wire Line
	6350 2750 6350 3200
Text Label 12950 1650 0    50   ~ 0
PE1
Text Label 14500 2050 0    50   ~ 0
PB6
Text Label 4950 8950 1    50   ~ 0
PF9
Text Label 5050 8950 1    50   ~ 0
PF10
Text Label 5150 8950 1    50   ~ 0
PC0
Text Label 5250 8950 1    50   ~ 0
PA5
Text Label 5350 8950 1    50   ~ 0
PA6
Text Label 5450 8950 1    50   ~ 0
PB2
Text Label 5550 8950 1    50   ~ 0
PF11
Text Label 5650 8950 1    50   ~ 0
PF12
Text Label 5750 8950 1    50   ~ 0
PF13
Text Label 5850 8950 1    50   ~ 0
PF14
Text Label 5950 8950 1    50   ~ 0
PF15
Text Label 6050 8950 1    50   ~ 0
PG0
Text Label 6150 8950 1    50   ~ 0
PG1
Text Label 6250 8950 1    50   ~ 0
PE7
Text Label 6350 8950 1    50   ~ 0
PE8
Text Label 6450 8950 1    50   ~ 0
PE9
Text Label 6550 8950 1    50   ~ 0
PE10
Text Label 6650 8950 1    50   ~ 0
PE11
Text Label 6750 8950 1    50   ~ 0
PE12
Text Label 6850 8950 1    50   ~ 0
PE13
Text Label 6950 8950 1    50   ~ 0
PE14
Wire Wire Line
	7550 2750 7550 3200
Text Label 7550 3200 1    50   ~ 0
PE15
Text Label 7450 3200 1    50   ~ 0
PB14
Wire Wire Line
	7450 3200 7450 2750
Text Label 8150 3200 1    50   ~ 0
PB15
Text Label 8050 3200 1    50   ~ 0
PD8
Wire Wire Line
	8050 3200 8050 2750
Wire Wire Line
	8150 2750 8150 3200
Text Label 9550 8950 1    50   ~ 0
PD9
Text Label 9450 8950 1    50   ~ 0
PD10
Text Label 9350 8950 1    50   ~ 0
PD11
Text Label 9250 8950 1    50   ~ 0
PD12
Text Label 9150 8950 1    50   ~ 0
PD13
Text Label 9050 8950 1    50   ~ 0
PD14
Text Label 8950 8950 1    50   ~ 0
PD15
Text Label 8850 8950 1    50   ~ 0
PG2
Text Label 8750 8950 1    50   ~ 0
PG3
Text Label 8650 8950 1    50   ~ 0
PG4
Text Label 8550 8950 1    50   ~ 0
PG5
Text Label 8450 8950 1    50   ~ 0
PG6
Text Label 8350 8950 1    50   ~ 0
PG7
Text Label 8250 8950 1    50   ~ 0
PG8
Text Label 8150 8950 1    50   ~ 0
PC6
Text Label 8050 8950 1    50   ~ 0
PC7
Text Label 7950 8950 1    50   ~ 0
PC8
Text Label 7850 8950 1    50   ~ 0
PC9
Text Label 7750 8950 1    50   ~ 0
PA8
Text Label 7650 8950 1    50   ~ 0
PA9
Text Label 7550 8950 1    50   ~ 0
PA10
Wire Wire Line
	9250 3200 9250 2750
Wire Wire Line
	9350 2750 9350 3200
Wire Wire Line
	9450 3200 9450 2750
Wire Wire Line
	9550 2750 9550 3200
Wire Wire Line
	9650 2750 9650 3200
Wire Wire Line
	9750 2750 9750 3200
Wire Wire Line
	9850 2750 9850 3200
Wire Wire Line
	9950 2750 9950 3200
Text Label 9650 3200 1    50   ~ 0
PA11
Text Label 9750 3200 1    50   ~ 0
PA12
Text Label 9850 3200 1    50   ~ 0
PD0
Text Label 9950 3200 1    50   ~ 0
PD1
Text Label 9550 3200 1    50   ~ 0
PD2
Text Label 9450 3200 1    50   ~ 0
PD3
Text Label 9350 3200 1    50   ~ 0
PD4
Text Label 9250 3200 1    50   ~ 0
PD7
Wire Wire Line
	11050 3200 11050 2750
Wire Wire Line
	11150 2750 11150 3200
Wire Wire Line
	11250 2750 11250 3200
Wire Wire Line
	11350 2750 11350 3200
Wire Wire Line
	11450 2750 11450 3200
Wire Wire Line
	11550 2750 11550 3200
Wire Wire Line
	11650 2750 11650 3200
Wire Wire Line
	11750 2750 11750 3200
Text Label 11450 3200 1    50   ~ 0
PG9
Text Label 11550 3200 1    50   ~ 0
PG10
Text Label 11650 3200 1    50   ~ 0
PG11
Text Label 11750 3200 1    50   ~ 0
PG12
Text Label 11050 3200 1    50   ~ 0
PG13
Text Label 11150 3200 1    50   ~ 0
PG14
Text Label 11250 3200 1    50   ~ 0
PG15
Text Label 11350 3200 1    50   ~ 0
PB3
$Comp
L power:GND #PWR0169
U 1 1 5CB5B32C
P 13150 7500
F 0 "#PWR0169" H 13150 7250 50  0001 C CNN
F 1 "GND" H 13250 7400 50  0000 C CNN
F 2 "" H 13150 7500 50  0001 C CNN
F 3 "" H 13150 7500 50  0001 C CNN
	1    13150 7500
	1    0    0    -1  
$EndComp
Wire Wire Line
	12700 7450 13150 7450
Text Label 12000 7550 0    50   ~ 0
RST
Wire Wire Line
	12000 7550 12200 7550
$Comp
L power:+3.3V #PWR0170
U 1 1 5CCB6EDF
P 12100 7350
F 0 "#PWR0170" H 12100 7200 50  0001 C CNN
F 1 "+3.3V" V 12150 7400 50  0000 C CNN
F 2 "" H 12100 7350 50  0001 C CNN
F 3 "" H 12100 7350 50  0001 C CNN
	1    12100 7350
	0    -1   -1   0   
$EndComp
Wire Wire Line
	12100 7350 12200 7350
Wire Wire Line
	12200 7450 12000 7450
Text Label 12000 7450 0    50   ~ 0
SWDIO
Text Label 12950 7550 2    50   ~ 0
SWCLK
Wire Wire Line
	12700 7550 12950 7550
Wire Wire Line
	13150 7450 13150 7500
$Comp
L Graphic:SYM_Arrow45_Tiny SM1
U 1 1 5CB69321
P 12600 10050
F 0 "SM1" H 12760 10100 50  0001 C CNN
F 1 "SYM_Arrow45_Tiny" H 12600 9980 50  0001 C CNN
F 2 "Fiducials:Fiducial_0.75mm_Dia_1.5mm_Outer" H 12600 10050 50  0001 C CNN
F 3 "~" H 12600 10050 50  0001 C CNN
	1    12600 10050
	1    0    0    -1  
$EndComp
$Comp
L Graphic:SYM_Arrow45_Tiny SM3
U 1 1 5CB6962B
P 12650 10250
F 0 "SM3" H 12810 10300 50  0001 C CNN
F 1 "SYM_Arrow45_Tiny" H 12650 10180 50  0001 C CNN
F 2 "LIB_KiCad:Symbol_kurzschlussblog" H 12650 10150 50  0000 C CNN
F 3 "~" H 12650 10250 50  0001 C CNN
	1    12650 10250
	1    0    0    -1  
$EndComp
$Comp
L Graphic:SYM_Arrow45_Tiny SM2
U 1 1 5CB69C5B
P 12650 10050
F 0 "SM2" H 12810 10100 50  0001 C CNN
F 1 "SYM_Arrow45_Tiny" H 12650 9980 50  0001 C CNN
F 2 "Fiducials:Fiducial_0.75mm_Dia_1.5mm_Outer" H 12650 10050 50  0001 C CNN
F 3 "~" H 12650 10050 50  0001 C CNN
	1    12650 10050
	1    0    0    -1  
$EndComp
$Comp
L Graphic:SYM_Arrow45_Tiny SM4
U 1 1 5CB69D20
P 12700 10050
F 0 "SM4" H 12860 10100 50  0001 C CNN
F 1 "SYM_Arrow45_Tiny" H 12700 9980 50  0001 C CNN
F 2 "Fiducials:Fiducial_0.75mm_Dia_1.5mm_Outer" H 12700 9950 50  0000 C CNN
F 3 "~" H 12700 10050 50  0001 C CNN
	1    12700 10050
	1    0    0    -1  
$EndComp
$Comp
L Graphic:SYM_Arrow45_Tiny SM5
U 1 1 5CC19353
P 14650 10050
F 0 "SM5" H 14810 10100 50  0001 C CNN
F 1 "SYM_Arrow45_Tiny" H 14650 9980 50  0001 C CNN
F 2 "Symbols:ESD-Logo_22x20mm_SilkScreen" H 14650 9950 50  0000 C CNN
F 3 "~" H 14650 10050 50  0001 C CNN
	1    14650 10050
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Male J3
U 1 1 5CB7E2EB
P 14450 650
F 0 "J3" V 14350 500 100 0000 L CNN
F 1 "Conn_01x02_Male" V 14639 690 50  0001 L CNN
F 2 "Connectors_Molex:Molex_KK-6410-02_02x2.54mm_Straight" H 14450 650 50  0001 C CNN
F 3 "~" H 14450 650 50  0001 C CNN
	1    14450 650 
	0    1    1    0   
$EndComp
Wire Wire Line
	14350 850  14350 1150
Wire Wire Line
	14450 850  14450 950 
Wire Wire Line
	13850 950  14450 950 
Connection ~ 14450 950 
Wire Wire Line
	14450 950  15400 950 
Wire Wire Line
	15400 950  15400 1150
Wire Wire Line
	13850 950  13850 1150
Wire Bus Line
	12500 3350 12500 3300
Wire Bus Line
	12500 3850 12500 3800
Wire Bus Line
	12500 4300 12500 4350
Wire Bus Line
	12500 4800 12500 4850
Wire Bus Line
	12500 5300 12500 5350
Wire Bus Line
	12500 5800 12500 5850
Wire Wire Line
	5550 7000 5550 7850
Wire Wire Line
	5550 7000 5900 7000
Wire Wire Line
	5550 7850 5750 7850
Wire Wire Line
	4550 7000 4550 6950
Wire Wire Line
	2750 10850 2800 10850
Wire Wire Line
	2750 10650 2800 10650
Wire Wire Line
	5750 9150 5700 9150
Wire Wire Line
	5700 9150 5700 9000
Wire Wire Line
	5700 9000 5750 9000
Wire Wire Line
	5750 9150 5750 9200
Wire Wire Line
	5850 9000 5900 9000
Wire Wire Line
	5900 9000 5900 9150
Wire Wire Line
	5900 9150 5850 9150
Wire Wire Line
	5850 9150 5850 9200
Wire Wire Line
	1550 1200 1550 650 
Connection ~ 1550 1200
Wire Wire Line
	1550 650  2400 650 
Wire Wire Line
	1550 1200 1550 1800
Connection ~ 1550 1800
$EndSCHEMATC
